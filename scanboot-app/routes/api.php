<?php

use Illuminate\Http\Request;
use App\Http\Controllers\Credits;
use App\Http\Controllers\Devices;
use App\Http\Controllers\Admin\Tags;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardCreditsView;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', [AuthController::class, 'login']);

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' => ['auth:sanctum', 'verified']], function () {
    Route::get('logout', [AuthController::class, 'logout']);
    Route::group(['middleware' => ['access.manager']], function () {
        Route::resource('scans', Credits::class);
        Route::get('dashboard/{deviceid}', [Credits::class, 'getByDeviceId']);
        Route::resource('tags', Tags::class);
        Route::get('devices', [Devices::class, 'index']);
        Route::resource('register-scan', Credits::class);
    });
});


