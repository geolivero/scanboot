<?php

use App\Http\Controllers\Pages;
use App\Http\Livewire\Admin\Tag;
use App\Http\Livewire\Admin\Tags;
use App\Http\Livewire\Admin\Users;
use App\Http\Livewire\Admin\Member;
use App\Http\Livewire\Admin\Company;
use App\Http\Livewire\Admin\Exports;
use App\Http\Livewire\Admin\Members;
use Illuminate\Support\Facades\Route;
use App\Http\Livewire\Admin\Companies;
use App\Http\Livewire\Admin\Dashboard;
use App\Http\Livewire\Admin\Dashboards;
use App\Http\Livewire\Admin\Users\User;
use App\Http\Livewire\CreditsDashboard;
use Illuminate\Support\Facades\Artisan;
use App\Http\Livewire\Admin\Corporation;
use App\Http\Livewire\Admin\Departement;
use App\Http\Livewire\Admin\Corporations;
use App\Http\Livewire\Admin\Departements;
use App\Http\Livewire\Admin\DeviceLocation;
use App\Http\Livewire\Admin\CreditsCategory;
use App\Http\Livewire\Admin\DeviceLocations;
use App\Http\Controllers\DashboardCreditsView;
use App\Http\Livewire\Admin\CreditsCategories;


Route::get('/', function () {
    return view('home');
});



Route::get('/quote', function () {
    return view('home');
})->name('quote');
Route::get('/credits-dashboard', CreditsDashboard::class)->name('credits.dashboard');
Route::get('/credits-dashboard/view', [DashboardCreditsView::class, 'index'])->name('credits.dashboard.view');

Route::get('/page/{page}', [Pages::class, 'show'])->name('pages');
Route::get('/credits-dashboard/json', [DashboardCreditsView::class, 'show']);


Route::group(['middleware' => ['auth:sanctum', 'verified']], function () {

    Route::get('/clear-cache', function() {
        Artisan::call('cache:clear');
        session()->flash('message', __('Cache has been cleared'));
        return redirect()->to(route('dashboard'));

    })->name('clear.cache');

    Route::group(['middleware' => ['access.admin']], function () {
        Route::get('/admin/users', Users::class)->name('admin.users');
        Route::get('/admin/user/{id}', User::class)->name('admin.users.user');
        Route::get('/admin/user', User::class)->name('admin.users.new');
        Route::get('/admin/send/welcome/{id}', User::class)->name('admin.welcome');
        Route::get('/admin/corporations', Corporations::class)->name('admin.corporations');
        Route::get('/admin/corporations/corporation', Corporation::class)->name('admin.corporations.corporation');
        Route::get('/admin/corporations/corporation/{id}', Corporation::class)->name('admin.corporations.corporation.edit');
        Route::get('/admin/device-location', DeviceLocation::class)->name('admin.devicelocation');
        Route::get('/admin/device-location/{id}', DeviceLocation::class)->name('admin.devicelocation.edit');

    });

    Route::group(['middleware' => ['access.manager']], function () {
        Route::get('/dashboard', Dashboard::class)->name('dashboard');
        Route::get('/admin/dashboards', Dashboards::class)->name('admin.dashboards');
        Route::get('/admin/companies', Companies::class)->name('admin.companies');
        Route::get('/admin/companies/company', Company::class)->name('admin.companies.company');
        Route::get('/admin/companies/company/{id}', Company::class)->name('admin.companies.company.edit');
        Route::get('/admin/departments', Departements::class)->name('admin.departements');
        Route::get('/admin/departments/department', Departement::class)->name('admin.departements.departement');
        Route::get('/admin/departments/department/{id}', Departement::class)->name('admin.departements.departement.edit');
        Route::get('/admin/credits', CreditsCategories::class)->name('admin.credits');
        Route::get('/admin/credits/credit', CreditsCategory::class)->name('admin.credits.credit');
        Route::get('/admin/credits/credit/{id}', CreditsCategory::class)->name('admin.credits.credit.edit');
        Route::get('/admin/members', Members::class)->name('admin.members');
        Route::get('/admin/members/member', Member::class)->name('admin.members.member');
        Route::get('/admin/members/member/{id}', Member::class)->name('admin.members.member.edit');
        Route::get('/admin/tags', Tags::class)->name('admin.tags');
        Route::get('/admin/device-locations', DeviceLocations::class)->name('admin.devicelocations');
        Route::get('/admin/tags/tag', Tag::class)->name('admin.tags.tag');
        Route::get('/admin/tags/tag/{id}', Tag::class)->name('admin.tags.tag.edit');
        Route::get('/admin/exports', Exports::class)->name('admin.exports');
    });

    Route::get('/not-allowed', function () {
        return view('admin.notallowed');
    })->name('access.notallowed');

});
