<x-layout.head />
    <div class="max-w-1120 mx-auto">
        <header class="main fixed transition-all duration-1000 w-full left-0 top-0 h-32">
            <nav class="md:flex justify-between max-w-1120 mx-auto items-center px-4 py-5">
                <a href="/" class="flex my-2 md:my-0 justify-center md:block">
                    <img src="/assets/imgs/logo_web.svg" alt="scanboot"/>
                </a>
                <ul class="flex">
                    <li><a class="inline-flex p-2 mx-2 text-mineral text-xl md:text-2xl uppercase font-bold hover:text-main transition-all" href="{{ route('pages', ['page' => 'contact']) }}">Get a quote</a></li>
                    <li><a class="inline-flex p-2 mx-2 text-mineral text-xl md:text-2xl uppercase font-bold hover:text-main transition-all" href="{{ route('pages', ['page' => 'faq']) }}">FAQ</a></li>
                    <li><a class="inline-flex p-2 mx-2 text-mineral text-xl md:text-2xl uppercase font-bold hover:text-main transition-all" href="{{ route('pages', ['page' => 'contact']) }}">Contact</a></li>
                    <li><a class="bg-white rounded shadow-md inline-flex p-2 mx-2 text-mineral text-xl md:text-2xl uppercase font-bold hover:text-main transition-all" href="{{ url('/login') }}">Login</a></li>
                </ul>
            </nav>
        </header>
        <main>
            {{ $slot }}
        </main>
    </div>
    <footer class="min-h-500 bg-mineral">
        <div class="max-w-1120 mx-auto  text-white">
            <div class="py-40 md:grid grid-cols-2 gap-10">
                <div class="md:flex justify-end">
                    <div class="text-right">
                        <img src="/assets/imgs/logo_footer.svg" class="flex-shrink-0" alt="Scanboot"/>
                    </div>
                </div>
                <div>
                    <ul>
                        <li class="font-light"><a class="hover:text-main" href="/">Home</a></li>
                        <li class="font-light"><a class="hover:text-main" href="{{ route('pages', ['page' => 'contact']) }}">Contact</a></li>
                        <li class="font-light"><a class="hover:text-main" href="{{ route('pages', ['page' => 'support']) }}">Support</a></li>
                        <li class="font-light"><a class="hover:text-main" href="{{ route('pages', ['page' => 'terms']) }}">Terms and condition</a></li>
                    </ul>
                </div>
            </div>
            <div>
                <span class="font-light">All rights reserved to Scanboot {{ date('Y') }}</span>
            </div>
        </div>
    </footer>
<x-layout.footer />
