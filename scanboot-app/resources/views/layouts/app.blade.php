
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">

        @livewireStyles

        <!-- Scripts -->

        <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32" />
        <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16" />

        <link href="https://cdn.jsdelivr.net/npm/tom-select@2.0.0-rc.4/dist/css/tom-select.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/pikaday/css/pikaday.css">

        <script src="https://cdn.jsdelivr.net/npm/tom-select@2.0.0-rc.4/dist/js/tom-select.complete.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/pikaday/pikaday.js"></script>

        <script src="{{ mix('js/app.js') }}" defer></script>
    </head>
    <body class="font-sans antialiased">
        <x-jet-banner />

        <div class="min-h-screen bg-gray-100">
            @livewire('navigation-menu')

            <!-- Page Heading -->
            @if (isset($header))
                <header class="bg-white shadow ">
                    <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
                        {{ $header }}
                    </div>
                </header>
            @endif

            <!-- Page Content -->
            <main class="md:grid md:grid-cols-13 max-w-7xl mx-auto">
                <nav class="md:col-start-1 md:col-end-4 pr-3 md:py-12 pl-4">

                    <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg min-h-10">
                        <div class="py-2 px-4">
                            <a class="underline text-sm text-gray-300" href="{{ route('clear.cache') }}">
                                {{ __('clear cache') }}
                            </a>
                        </div>
                        <ul class="py-2">
                            <li class="border-b border-gray-100">
                                <x-nav-icon type="dashboard" url="dashboard">Dashboard</x-nav-icon>
                            </li>
                            @if (\Admin::isRole('Admin'))
                            <li>
                                <x-nav-icon type="corporation" url="{{ route('admin.corporations') }}">{{ __('Corporation') }}</x-nav-icon>
                            </li>
                            <li>
                                <x-nav-icon type="users" url="{{ route('admin.users') }}">Users</x-nav-icon>
                            </li>
                            @endif
                            @if (\Admin::isRole('Manager'))
                            <li class=" mt-2 pt-2 border-t border-gray-300">
                                <x-nav-icon type="dashboard" url="{{ route('admin.dashboards') }}">Dashboards</x-nav-icon>
                            </li>
                            <li>
                                <x-nav-icon type="corporation" url="{{ route('admin.companies') }}">{{ __('Companies') }}</x-nav-icon>
                            </li>
                            <li>
                                <x-nav-icon type="money" url="{{ route('admin.credits') }}">
                                    {{ __('Define Credits') }}
                                </x-nav-icon>
                            </li>
                            <li>
                                <x-nav-icon type="location" url="{{ route('admin.devicelocations') }}">Device location's</x-nav-icon>
                            </li>
                            <li>
                                <x-nav-icon type="tag" url="{{ route('admin.tags') }}">Tags</x-nav-icon>
                            </li>
                            <li class="mt-2 pt-2 border-t border-gray-300 ">
                                <x-nav-icon type="users" url="{{ route('admin.members') }}">Members</x-nav-icon>
                            </li>

                            <li class="mt-2 pt-2 border-t border-gray-300 ">
                                <x-nav-icon type="stats" url="{{ route('admin.exports') }}">Exports</x-nav-icon>
                            </li>
                            @endif

                        </ul>
                    </div>
                </nav>
                <div class="md:col-start-4 md:col-end-13">
                    {{ $slot }}
                </div>
            </main>
        </div>

        @stack('modals')
        @livewireScripts


    </body>
</html>
