<x-layout.head />

{{ $slot }}

@stack('modals')
@livewireScripts
<x-layout.footer />
