<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Users') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="px-4">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="p-4">
                    <x-ui.btn
                        label="{{ __('Create user') }}"
                        url="{{ route('users.create') }}"
                        />
                </div>
                <x-layout.listwrapper>
                    @foreach ($users as $user)
                        <x-layout.row-item
                            label="{{ $user->name }}"
                            url="{{ route('users.edit', $user) }}"
                            date="{{ $user->created_at->format('m/d/Y H:i') }}"
                            id="{{ $user->id }}"
                            last="{{ $loop->last }}"
                            />
                    @endforeach
                </x-layout.listwrapper>
            </div>
        </div>
    </div>
</x-app-layout>
