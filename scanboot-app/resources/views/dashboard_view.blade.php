<x-layout.head />
<div class="bg-light">
    <div class=" min-h-screen flex justify-center items-center w-full max-w-5xl px-4 mx-auto">
        <header class="main"></header>
        <div class="min-h-full flex flex-col items-center justify-center w-full">
            <div>
                <div class="current_scan " class="flex flex-col items-center">
                    <template id="current_user">
                        <span class="p-4 block device text-xl text-center">{device}</span>
                        <span class="rounded-full mx-auto mb-4 text-6xl text-green-400 total bg-white w-32 h-32 flex items-center justify-center">{username}</span>
                        <span class="name text-center block text-2xl">{scanned}</span>
                    </template>
                </div>
            </div>
            <div class="flex flex-1 justify-start flex-col w-full">
                <h3 class="p-4 text-gray-400">{{ __('Latest') }}</h3>
                <div class="grid grid-cols-9 rows gap-3">
                    <template id="row">
                        <div class="col-span-4 bg-white p-4">{name}</div>
                        <div class="col-span-1 bg-white p-4">{total}</div>
                        <div class="col-span-4 bg-white p-4">{date}</div>
                    </template>
                </div>
            </div>

        </div>

    </div>
</div>
<script>
    (function () {
        let timer;
        const curr = document.querySelector('.current_scan');
        const curr_templ = document.querySelector('#current_user');
        const rows = document.querySelector('.rows');
        const row = document.querySelector('#row');
        let results = { credits: [], current: {}};

        const refetch = () => {
            timer = setTimeout(() => {
                get();
                clearTimeout(timer);
            }, 1000);
        }
        function render() {
            const clonedCurrent = curr_templ.content.cloneNode(true);
            curr.innerHTML = '';
            const total = clonedCurrent.querySelector('.total');
            const name = clonedCurrent.querySelector('.name');
            const device = clonedCurrent.querySelector('.device');
            total.textContent = results.current.credits;
            name.textContent = results.current.name;
            device.textContent = results.device;
            curr.appendChild(device);
            curr.appendChild(total);
            curr.appendChild(name);
            rows.innerHTML = '';
            results.credits.forEach( scans => {
                const clone = row.content.cloneNode(true);
                const spans = clone.querySelectorAll('div');
                spans[0].textContent = scans.name;
                spans[1].textContent = scans.total;
                spans[2].textContent = scans.date;

                [...spans].forEach( scEl => {
                    rows.appendChild(scEl);
                });

            });

        }
        async function get () {
            const api = '/credits-dashboard/json';
            const resp = await fetch(api);
            results = await resp.json();
            render();
            refetch();
        }
        get();
    })();
</script>
<x-layout.footer />
