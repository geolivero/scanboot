<x-base-layout>
    <main>
        <header class="md:grid grid-cols-2 px-4 pb-16">
            <div class="pt-52 md:pt-32 md:flex flex-col justify-center">
                <h1 class="text-6xl text-mineral font-bold pb-7">
                    Your <span class="text-main">credits</span> manager
                </h1>
                <p class="text-3xl text-dark leading-10 pb-4 font-light">
                    NFC technology to accurately scan users on a mobile station.
                </p>
                <div class="py-5">
                    <a class="hover:bg-main transition-all hover:text-mineral rounded-lg text-xl px-8 py-4 bg-mineral text-white uppercase inline-block" href="{{ route('pages', ['page' => 'contact']) }}">
                        Get a quote
                    </a>
                </div>
            </div>
            <div class="flex justify-end">
                <img class="hidden md:block w-full -mt-2" src="/assets/imgs/right_coll.webp" alt="Scanboot" />
            </div>

        </header>
        <div class="md:grid grid-cols-2 gap-10 px-4">
            <div class="flex justify-start">
                <img class="w-full -ml-2 pb-4" src="/assets/imgs/left_collom.webp" alt="Scanboot" />
            </div>
            <div class="pt-10">
                <h2 class="text-5xl text-mineral font-bold pb-7">I need a way to track our community traffic</h2>
                <p class="text-xl text-dark leading-10 pb-4 font-light">With NFC technology we can track the total credits used for mutliple activities.</p>
                <p class="text-xl text-dark leading-10 pb-4 font-light">The solution to track, memberships, cafeterias, spa, salons etc.</p>

            </div>
        </div>
    </main>
</x-base-layout>
