<div class="md:grid md:grid-cols-12 @if(!$last)border-b border-gray-200 first-letter:@endif py-2">
    <div class="col-start-1 col-end-2">
        {{ $id }}
    </div>
    <div class="col-start-2 col-end-10">
        <x-layout.ui.anker
            url="{{ $url }}"
            label="{{ $label }}"
        />
    </div>
    <div class="col-start-10 col-end-13">
        {{ $date }}
    </div>
</div>
