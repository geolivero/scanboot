<label for="{{ $id }}">{{ $label }}</label>
<input
    class="ml-2 rounded-sm w-full p-2 border border-gray-300"
    wire:model.lazy="{{ $id }}"
    type="datetime-local" id="{{ $id }}" />
