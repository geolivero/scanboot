<a href="{{ $url }}" class="text-gray-900 block py-2 px-2 grid grid-cols-6 justify-center gap-2">
    {{ $type }}
    <span>{{ $slot }}</span>
</a>
