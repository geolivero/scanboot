<div class="p-4 grid grid-4 gap-2">
    <div class="col-start-1 col-end-3">
        <input wire:model="search" class="w-full" type="search" placeholder="{{ __('Search')}}..." />
    </div>
    <div class="col-start-3 col-end-4">
        <select wire:model="sort" class="w-full">
            <option value="">{{ __('Sort by field') }}</option>
            @forelse ($filters as $filter)
                <option value="{{ $filter['value'] }}">{{ $filter['label'] }}</option>
            @empty

            @endforelse
            <option value="created_at">{{ __('Created at') }}</option>
        </select>
    </div>
    <div class="col-start-4 col-end-5">
        <select wire:model="sort_type" class="w-full">
            <option value="">{{ __('Sort type') }}</option>
            <option value="0">Asc</option>
            <option value="1">Desc</option>
        </select>
    </div>
</div>
