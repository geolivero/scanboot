<a
    class="rounded-lg bg-indigo-800 text-white inline-flex h-8 px-5 items-center hover:bg-indigo-500"
    href="{{ $url }}">
    {{ $label }}
</a>
