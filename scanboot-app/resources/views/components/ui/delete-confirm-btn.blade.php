<div class="d-inline" x-data="{ confirmDelete:false }">
    <span
        x-show="!confirmDelete"
        x-on:click="confirmDelete=true"
        class="inline-block cursor-pointer">
        <x-ui.trash-icon />
    </span>


     <span
        x-show="confirmDelete"
        x-on:click="confirmDelete=false"
        wire:click="confirmRemoval({{ $id }})"
        class="inline-block px-2 text-red-500 cursor-pointer">Yes</span>

     <span
        x-show="confirmDelete"
        x-on:click="confirmDelete=false"
        class="inline-block px-2 text-green-600 cursor-pointer">No</span>
 </div>
