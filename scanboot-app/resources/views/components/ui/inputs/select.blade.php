<div class="py-2 flex flex-col">
    <label class="block pb-2" for="{{ $name }}">{{ $label }}</label>
    <select {!! $attributes->merge(['id' => $name, ]) !!}>
        <option value="0">{{ __('Choose a value') }}</option>
        @foreach ($values as $value)
        <option value="{{ $value->id }}">
            {{ $value->name }}
        </option>
        @endforeach
    </select>
    @error($name)
    <p class="error text-red-400 py-2">
        {{ $message }}
    </p>
    @enderror
</div>
