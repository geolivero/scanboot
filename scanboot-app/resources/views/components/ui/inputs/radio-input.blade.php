<div class="py-3 flex flex-col">
    <p class="font-bold pb-2">{{ $label }}</p>
    @foreach ($rows as $row)
        <label class="cursor-pointer pb-2 flex items-center" for="{{ $name }}_{{ $loop->index }}">
            <input type="radio" id="{{ $name }}_{{ $loop->index }}" name="{{ $name }}" value="{{ $row['value'] }}" />
            <span class="ml-2">{{ $row['label'] }}</span>
        </label>
    @endforeach
</div>
