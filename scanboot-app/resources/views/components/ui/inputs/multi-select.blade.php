<div wire:ignore>
    <select id="{{ $id }}" autocomplete="off" placeholder="{{ __('Choose an option') }}" multiple class="multi_select">
    @if($values)
    @forelse($values as $value)
        <option @if(array_search($value->{$identifier}, $selectedOptions) > -1) selected @endif value="{{ $value->{$identifier} }}">
            {{ $value->name }}
        </option>
    @empty
        <option value="0">{{ __('No options yet, create one!') }}</option>
    @endforelse
    @endif
    </select>
</div>

@error($selectedLabel)
<p class="error text-red-400 py-2">
    {{ $message }}
</p>
@enderror

<script>
    new TomSelect('#{{ $id }}', {
        onChange: function (value) {
            @this.set('{{ $selectedLabel }}', value);
        }
    });
</script>
