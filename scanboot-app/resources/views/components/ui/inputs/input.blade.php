<div class="py-2 flex flex-col">
    <label for="{{ $name }}">{{ $label }}</label>
    @props(['disabled' => false])
    <input {{ $disabled ? 'disabled' : '' }} {!! $attributes->merge(['class' =>
        'border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm',
        'type' => $type,
        'id' => $name,
        'placeholder' => $placeholder
    ]) !!}>

    @error($name)
    <p class="error text-red-400 py-2">
        {{ $message }}
    </p>
    @enderror
</div>

