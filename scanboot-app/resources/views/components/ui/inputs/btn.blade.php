<button wire:loading.attr="disabled" class="inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:ring focus:ring-gray-300 disabled:opacity-25 transition text-center max-w-max cursor-pointer" />
    <span wire:loading>
        <x-ui.loader-icon />
    </span>
    {{  $label }}
</button>
