<div class="py-3 flex flex-col">
    <p class="font-bold pb-2">{{ $label }}</p>
    @foreach($rows as $key => $row)
        <label class="cursor-pointer pb-2 flex items-center" for="{{ $name }}_{{ $loop->index }}">
            <input wire:model="{{ $model }}.{{ $key }}" type="checkbox" id="{{ $name }}_{{ $loop->index }}" value="{{ $row['value'] }}" />
            <span class="ml-2">{{ $row['label'] }}</span>
        </label>
    @endforeach
    @error($name)
    <p class="error text-red-400 py-2">
        {{ $message }}
    </p>
    @enderror
</div>
