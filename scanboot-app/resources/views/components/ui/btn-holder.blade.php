<div class="border-t border-gray-100 pt-4 mt-2 grid grid-cols-5 gap-4">
    {{  $slot  }}
</div>
