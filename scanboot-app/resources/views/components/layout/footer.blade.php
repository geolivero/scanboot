<script>
    const header = document.querySelector('header.main');
    const sticky = header.offsetTop;
    window.onscroll = function() {
        header.classList[window.pageYOffset > sticky ? 'add' : 'remove']('bg-white', 'shadow-lg');
    };
</script>
</body>
</html>
