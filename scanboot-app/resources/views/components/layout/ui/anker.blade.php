<a class="text-indigo-500 inline-block px-2 hover:bg-indigo-800 hover:text-white" href="{{ $url }}">
    {{ $label }}
</a>
