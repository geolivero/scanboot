<x-base-layout>
    <div class="px-4 pt-36 content">
        <h2 class="text-5xl text-mineral font-bold pb-7">Contact</h2>
        <p><a href="mailto: question@scanboot.app">Send a message</a></p>
        <div class="h-14"></div>
    </div>
</x-base-layout>
