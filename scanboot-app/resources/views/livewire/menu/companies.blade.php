<ul class="ml-2 pl-2">
    @forelse ($companies as $company)
        <li>
            <a href="{{ route('admin.companies.departments', ['id' => $company->corporation_id]) }}"> - {{ $company->corporation->name }}</a>
        </li>
    @empty
        <li>

        </li>
    @endforelse
    {{-- If you look to others for fulfillment, you will never truly be fulfilled. --}}
</ul>
