<div class="bg-light min-h-screen flex justify-center items-center">
    <div class="mx-auto max-w-sm flex-1 py-10 px-4 bg-white rounded-lg shadow-md">
        <form class="p-4" wire:submit.prevent="access" action="{{ route('credits.dashboard') }}" method="post">
            <x-ui.inputs.input
                type="text"
                name="token"
                placeholder="{{ __('Access token') }}"
                label="{{ __('') }}"
                wire:model.defer="token"
                />
            @if (session()->has('error'))
                <div>
                    <p class="text-red-400 py-2">
                        {{ session('error') }}
                    </p>
                </div>
            @endif
            <div class="justify-items-end text-right">
                <button  class="inline-flex items-center px-4 py-2 bg-gray-800 border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest hover:bg-gray-700 active:bg-gray-900 focus:outline-none focus:border-gray-900 focus:ring focus:ring-gray-300 disabled:opacity-25 transition text-center max-w-max cursor-pointer" />
                </span>
                    {{  __('Get access') }}
                </button>
            </div>
        </form>
    </div>
</div>
