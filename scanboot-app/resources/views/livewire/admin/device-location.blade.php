<x-backend-module-wrapper
    title="{{ __('Device Location') }}"
    >

        @if ($device && !is_array($device) && isset($device->id))
        <form class="p-4" wire:submit.prevent="update" action="{{ route('admin.devicelocation.edit', $device->id) }}" method="post">
        @else
        <form class="p-4" wire:submit.prevent="create" action="{{ route('admin.devicelocation') }}" method="post">
        @endif
            @method('PUT')
            @csrf
            <x-layout.ui.anker
                url="{{ route('admin.devicelocations') }}"
                label="{{ __('Go back') }}"
            />

            @if (session()->has('message'))
                <div>
                    <p class="text-green-400 py-2">
                        {{ session('message') }}
                    </p>
                </div>
            @endif


            <x-ui.inputs.input
                type="text"
                name="name"
                placeholder="{{ __('device name') }}"
                label="{{ __('Name') }}"
                wire:model.defer="device.name"
                />



            <x-ui.btn-holder>

                <div class="col-span-4 col-start-1 col-end-5">
                    @if ($device && !is_array($device) && isset($device->id))
                    <x-ui.delete-confirm-btn
                        id="{{ $device->id }}"
                        />
                    @endif
                </div>

                <div class="justify-items-end text-right">
                    <x-ui.inputs.btn
                        class=""
                        label="{{ __('Save') }}"
                        />
                </div>



            </x-ui.btn-holder>
        </form>
</x-backend-module-wrapper>
