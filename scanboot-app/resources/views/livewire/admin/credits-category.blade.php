<x-backend-module-wrapper
    title="{{ __('Credit') }}"
    >

        @if ($credit && !is_array($credit) && isset($credit->id))
        <form class="p-4" wire:submit.prevent="update" action="{{ route('admin.credits.credit', $credit->id) }}" method="post">
        @else
        <form class="p-4" wire:submit.prevent="create" action="{{ route('admin.credits.credit') }}" method="post">
        @endif
            @method('PUT')
            @csrf
            <x-layout.ui.anker
                url="{{ route('admin.credits') }}"
                label="{{ __('Go back') }}"
            />

            @if (session()->has('message'))
                <div>
                    <p class="text-green-400 py-2">
                        {{ session('message') }}
                    </p>
                </div>
            @endif

            <x-ui.inputs.input
                type="text"
                name="credit.name"
                placeholder="{{ __('credit name') }}"
                label="{{ __('Name') }}"
                wire:model.defer="credit.name"
                />
            <x-ui.inputs.input
                type="text"
                name="credit.credits"
                placeholder="{{ __('Credits amount') }}"
                label="{{ __('Credits') }}"
                wire:model.defer="credit.credits"
                />


            <x-ui.btn-holder>

                <div class="col-span-4 col-start-1 col-end-5">
                    @if ($credit && !is_array($credit) && isset($credit->id))
                    <x-ui.delete-confirm-btn
                        id="{{ $credit->id }}"
                        />
                    @endif
                </div>

                <div class="justify-items-end text-right">
                    <x-ui.inputs.btn
                        class=""
                        label="{{ __('Save') }}"
                        />
                </div>



            </x-ui.btn-holder>
        </form>
</x-backend-module-wrapper>
