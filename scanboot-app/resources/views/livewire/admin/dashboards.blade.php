<x-backend-module-wrapper
    title="{{ __('Dashboards') }}"
    >

    <div class="p-4 border-b border-gray-300 md:grid md:grid-cols-2 gap-5">

        <div>
            <select name="device_lication" wire:model="deviceLocationId" id="">
                <option value="0">{{ __(' - Choose a device - ') }}</option>
                @foreach ($deviceLocations as $location)
                    <option value="{{ $location->id }}">{{ $location->name }}</option>
                @endforeach
            </select>
            @if($deviceLocationId > 0)
            <a
                wire:click="createToken"
                class="rounded-lg bg-indigo-800 text-white inline-flex h-8 px-5 items-center hover:bg-indigo-500"
                href="#">
                Create new token
            </a>
            @endif
        </div>
        <p class="flex items-center">
            <svg version="1.1" class="w-9 h-9 text-indigo-400 fill-current" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 122.9 85.6" style="enable-background:new 0 0 122.9 85.6" xml:space="preserve"><g><path class="st0" d="M7.5,0h107.9c4.1,0,7.5,3.4,7.5,7.5v70.6c0,4.1-3.4,7.5-7.5,7.5H7.5c-4.1,0-7.5-3.4-7.5-7.5V7.5 C0,3.4,3.4,0,7.5,0L7.5,0z M69.9,63.3h28.5v4H69.9V63.3L69.9,63.3z M69.9,53.1H109v4H69.9V53.1L69.9,53.1z M92.1,35h5.6 c0.3,0,0.5,0.2,0.5,0.5v11c0,0.3-0.2,0.5-0.5,0.5h-5.6c-0.3,0-0.5-0.2-0.5-0.5v-11C91.6,35.3,91.8,35,92.1,35L92.1,35L92.1,35z M70.5,28.3h5.6c0.3,0,0.5,0.2,0.5,0.5v17.8c0,0.3-0.2,0.5-0.5,0.5h-5.6c-0.3,0-0.5-0.2-0.5-0.5V28.8 C69.9,28.5,70.2,28.3,70.5,28.3L70.5,28.3L70.5,28.3L70.5,28.3z M81.3,24.5h5.6c0.3,0,0.5,0.2,0.5,0.5v21.6c0,0.3-0.2,0.5-0.5,0.5 h-5.6c-0.3,0-0.5-0.2-0.5-0.5V25C80.8,24.7,81,24.5,81.3,24.5L81.3,24.5L81.3,24.5z M39.3,48.2l17,0.3c0,6.1-3,11.7-8,15.1 L39.3,48.2L39.3,48.2L39.3,48.2z M37.6,45.3l-0.2-19.8l0-1.3l1.3,0.1h0h0c1.6,0.1,3.2,0.4,4.7,0.8c1.5,0.4,2.9,1,4.3,1.7 c6.9,3.6,11.7,10.8,12.1,19l0.1,1.3l-1.3,0l-19.7-0.6l-1.1,0L37.6,45.3L37.6,45.3L37.6,45.3z M39.8,26.7L40,44.1l17.3,0.5 c-0.7-6.8-4.9-12.7-10.7-15.8c-1.2-0.6-2.5-1.1-3.8-1.5C41.7,27.1,40.8,26.9,39.8,26.7L39.8,26.7L39.8,26.7z M35.9,47.2L45.6,64 c-3,1.7-6.3,2.6-9.7,2.6c-10.7,0-19.4-8.7-19.4-19.4c0-10.4,8.2-19,18.6-19.4L35.9,47.2L35.9,47.2L35.9,47.2z M115.6,14.1H7.2v64.4 h108.4V14.1L115.6,14.1L115.6,14.1z"/></g></svg>
            <strong class="mx-2.5">
                Dashboard location: <a target="_blank" class="text-indigo-400 underline" href="{{ route('credits.dashboard') }}">dashboard</a>
            </strong>
        </p>
    </div>


    @if($dashboards && count($dashboards) > 0)

    <x-layout.listwrapper>
        @if($dashboards)
                <div class="grid grid-cols-6 items-center pb-2 border-b border-gray-400 gap-2">
                    <div class="col-span-3">{{ _('Token')  }}</div>
                    <div>{{ _('Device')  }}</div>
                </div>
            @forelse ($dashboards as $dashboard)
                <div class="grid grid-cols-6 items-center gap-2 pt-2">
                    <p class="col-span-3 p-2">
                        <span class="bg-black p-2 rounded-md text-white dash_copy">{{ $dashboard->token }}</span>
                    </p>
                    <p>{{ $dashboard->device->name }}</p>
                    <p>
                        <a href="#" wire:click="revoke({{ $dashboard->id }})" class="text-red-400">revoke</a>
                    </p>
                </div>
            @empty
                <p class="border border-red-400 p-4 my-4">{{ __('No results found') }}</p>
            @endforelse
        @else
            <p class="border border-red-400 p-4 my-4 flex">
                <svg class="w-5 h-5" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 115.4 122.88"><path d="M24.94,67.88A14.66,14.66,0,0,1,4.38,47L47.83,4.21a14.66,14.66,0,0,1,20.56,0L111,46.15A14.66,14.66,0,0,1,90.46,67.06l-18-17.69-.29,59.17c-.1,19.28-29.42,19-29.33-.25L43.14,50,24.94,67.88Z"/></svg>
                <span class="ml-4">
                {{ __('No results found, create a dashboard.') }}</span></p>
        @endif
    </x-layout.listwrapper>

    @endif

    <script>
        const cps = document.querySelector('.dash_copy');
        cps.addEventListener('click', e => {
            document.execCommand('copy');
        });

        cps.addEventListener('copy', function(event) {
            event.preventDefault();
            if (event.clipboardData) {
                event.clipboardData.setData('text/plain', cps.textContent);
                alert(`"${cps.textContent}", text is copied`);
               // console.log(event.clipboardData.getData('text'));
            }
        });
    </script>
</x-backend-module-wrapper>
