<x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        {{ __('Users') }}
    </h2>
</x-slot>

<div class="py-12">
    <div class="px-4">
        <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
            <div class="p-4">
                <x-ui.btn
                    label="{{ __('Create user') }}"
                    url="{{ route('admin.users.new') }}"
                    />
            </div>
            <x-ui.filters :filters="$filter_list" />
            <x-layout.listwrapper>
                @forelse ($users as $user)
                    <x-layout.row-item
                        label="{{ $user->name }}"
                        url="{{ route('admin.users.user', $user) }}"
                        date="{{ $user->created_at->format('m/d/Y H:i') }}"
                        id="{{ $user->id }}"
                        last="{{ $loop->last }}"
                        >
                        {{ $user->email }}
                    </x-layout.row-item>
                @empty
                    <p>{{ __('auth.no_results') }}</p>
                @endforelse
            </x-layout.listwrapper>
            <div class="p-4">
                {{ $users->links() }}
            </div>

        </div>


    </div>
</div>

