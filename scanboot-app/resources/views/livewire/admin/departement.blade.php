<x-backend-module-wrapper
    title="{{ __('Department') }}"
    >

        @if ($departement && !is_array($departement) && isset($departement->id))
        <form class="p-4" wire:submit.prevent="update" action="{{ route('admin.departements.departement', $departement->id) }}" method="post">
        @else
        <form class="p-4" wire:submit.prevent="create" action="{{ route('admin.departements.departement') }}" method="post">
        @endif
            @method('PUT')
            @csrf
            <x-layout.ui.anker
                url="{{ route('admin.departements') }}"
                label="{{ __('Go back') }}"
            />

            @if (session()->has('message'))
                <div>
                    <p class="text-green-400 py-2">
                        {{ session('message') }}
                    </p>
                </div>
            @endif

            <x-ui.inputs.select
                name="departement.company_id"
                label="{{ __('Company') }}"
                :values="companies()->get"
                wire:model.defer="departement.company_id"
                selectedOption="{{ $departement ?  $departement->company_id : 0 }}"
                />

            <x-ui.inputs.input
                type="text"
                name="departement.name"
                placeholder="{{ __('departement name') }}"
                label="{{ __('Name') }}"
                wire:model.defer="departement.name"
                />


            <x-ui.btn-holder>

                <div class="col-span-4 col-start-1 col-end-5">
                    @if ($departement && !is_array($departement) && isset($departement->id))
                    <x-ui.delete-confirm-btn
                        id="{{ $departement->id }}"
                        />
                    @endif
                </div>

                <div class="justify-items-end text-right">
                    <x-ui.inputs.btn
                        class=""
                        label="{{ __('Save') }}"
                        />
                </div>



            </x-ui.btn-holder>
        </form>
</x-backend-module-wrapper>
