<x-backend-module-wrapper
    title="{{ __('Corporation') }}"
    >
    @if ($corporation && !is_array($corporation) && isset($corporation->id))
        <form class="p-4" wire:submit.prevent="update" action="{{ route('admin.corporations.corporation', $corporation) }}" method="post">
        @else
        <form class="p-4" wire:submit.prevent="create" action="{{ route('admin.corporations.corporation') }}" method="post">
        @endif
            @method('PUT')
            @csrf
            <x-layout.ui.anker
                url="{{ route('admin.corporations') }}"
                label="{{ __('Go back') }}"
            />

            @if (session()->has('message'))
                <div>
                    <p class="text-green-400 py-2">
                        {{ session('message') }}
                    </p>
                </div>
            @endif


            <x-ui.inputs.input
                type="text"
                name="corporation.name"
                placeholder="{{ __('Corporation name') }}"
                label="{{ __('Name') }}"
                wire:model.defer="corporation.name"
                />

            <div>
                <label class="pb-2 block" for="timezones">{{ __('Timezone') }}</label>
                <select wire:model.defer="corporation.timezone" class="w-full"  id="timezones">
                    @foreach ($timezones as $key => $timezone)
                        <optgroup label="{{ $key }}">
                            @foreach ($timezone as $val => $zone)
                            <option value="{{ $val }}">{!! $zone !!}</option>
                            @endforeach
                        </optgroup>
                    @endforeach
                </select>
            </div>

            <x-ui.btn-holder>

                <div class="col-span-4 col-start-1 col-end-5">
                    @if ($corporation && !is_array($corporation) && isset($corporation->id))
                    <x-ui.delete-confirm-btn
                        id="{{ $corporation->id }}"
                        />
                    @endif
                </div>

                <div class="justify-items-end text-right">
                    <x-ui.inputs.btn
                        class=""
                        label="{{ __('Save') }}"
                        />
                </div>



            </x-ui.btn-holder>
        </form>
</x-backend-module-wrapper>
