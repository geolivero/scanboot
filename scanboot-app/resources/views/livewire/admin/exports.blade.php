<x-backend-module-wrapper
    title="{{ __('scans') }}"
    >
    @if(!empty($scans))
    <div class="p-4 border-b border-gray-300">
        {{-- <x-ui.btn url="#" label="{{ __('Download XLS') }}" /> --}}
        <button
            wire:click="export('csv')"
            class="bg-blue-500 flex items-center py-2 text-white font-bold uppercase text-xs px-4 rounded outline-none focus:outline-black hover:bg-blue-700">
            <svg class="fill-current mr-2 text-white w-4 h-4" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="92" height="92" viewBox="0 0 92 92" enable-background="new 0 0 92 92"><path id="XMLID_1335_" d="M89 58.8V86c0 2.8-2.2 5-5 5H8c-2.8.0-5-2.2-5-5V58.8c0-2.8 2.2-5 5-5s5 2.2 5 5V81h66V58.8c0-2.8 2.2-5 5-5S89 56 89 58.8zM42.4 65c.9 1 2.2 1.5 3.6 1.5s2.6-.5 3.6-1.5l19.9-20.4c1.9-2 1.9-5.1-.1-7.1-2-1.9-5.1-1.9-7.1.1L51 49.3V6c0-2.8-2.2-5-5-5s-5 2.2-5 5v43.3L29.6 37.7c-1.9-2-5.1-2-7.1-.1-2 1.9-2 5.1-.1 7.1L42.4 65z"
                /></svg> Download XLS
        </button>
    </div>
    @endempty
    <div wire:ignore class="pt-4 px-4 grid grid-cols-4 gap-2">
        <div>
            <label class="pb-2 block" for="tags">{{ __('Companies') }}</label>
            <x-ui.inputs.multi-select
                id="companies"
                selectedLabel="searched_companies"
                :values="$companies"
                :selectedOptions="[]"
                />
        </div>
        <div>
            <label class="pb-2 block" for="tags">{{ __('Tags') }}</label>
            <x-ui.inputs.multi-select
                id="tags"
                selectedLabel="searched_tags"
                :values="$tags"
                :selectedOptions="[]"
                />
        </div>
        <div>
            <label class="pb-2 block" for="members">{{ __('Members') }}</label>
            <x-ui.inputs.multi-select
                id="members"
                selectedLabel="selected_members"
                :values="$members"
                :selectedOptions="[]"
                />
        </div>
        <div>
            <label class="pb-2 block" for="members">{{ __('Devices') }}</label>
            <x-ui.inputs.multi-select
                id="devices"
                selectedLabel="selected_devices"
                :values="$devices"
                :selectedOptions="[]"
                />
        </div>

    </div>
    <div class="p-0 border-b border-gray-300 mb-4">
        <div class="md:grid md:grid-cols-12 gap-2 p-4">
            <div class="flex items-center col-span-4">
                <x-ui.pikaday
                    label="{{ __('From') }}"
                    id="date_from"
                    />
            </div>
            <div class="flex items-center col-span-4">
                <x-ui.pikaday
                    label="{{ __('To') }}"
                    id="date_to"
                    />
            </div>
            <div class="flex items-center col-span-2">
                <select wire:model="total_p_page" class="w-full">
                    <option value="">{{ __('Per page') }}</option>
                    <option value="20">20</option>
                    <option value="40">40</option>
                    <option value="60">60</option>
                    <option value="120">120</option>
                </select>
            </div>
            <div class="grid items-center col-span-2 gap-2 md:grid-cols-2">

                <select wire:model="sort" class="w-full">
                    <option value="">{{ __('Sort by field') }}</option>
                    @forelse ($filter_list as $filter)
                        <option value="{{ $filter['value'] }}">{{ $filter['label'] }}</option>
                    @empty

                    @endforelse
                    <option value="created_at">{{ __('Created at') }}</option>
                </select>
                <select wire:model="sort_type" class="w-full">
                    <option value="">{{ __('Sort type') }}</option>
                    <option value="0">Asc</option>
                    <option value="1">Desc</option>
                </select>
            </div>
        </div>
    </div>

    <div class="bg-light p-10 shadow-sm flex justify-end">
        <p class="text-2xl">
            <span>
            {{ __('Total:') }}
            </span>
            <strong class="text-green-400 text-4xl">
            {{ $total }}
            </strong>
        </p>
    </div>

    <x-layout.listwrapper>
        @if($scans)
            @forelse ($scans as $scan)
                <x-layout.row-item
                    label="{{ $scan->usersCredits->name }}"
                    url="{{ route('admin.members.member.edit', $scan->usersCredits) }}"
                    date="{{ $scan->created_at->format('m/d/Y g:i A') }}"
                    id="{{ $scan->id }}"
                    last="{{ $loop->last }}"
                    >
                    <div class="grid grid-cols-3 gap-2">
                        <div>
                         {{ $scan->total }}
                        </div>
                        <div>
                        {{ $scan->company->name }}
                        </div>
                        @if (\Admin::isRole('Admin'))
                        <div>
                            <button type="button"
                                wire:click="destroy({{ $scan->id }})"
                                class="text-red-400" data-toggle="modal"
                                data-target="#exampleModal">Delete</button>
                        </div>
                        @endif
                    </div>
                </x-layout.row-item>



            @empty
                <p class="border border-red-400 p-4 my-4">{{ __('No results found') }}</p>
            @endforelse
            <div class="py-4 mt-10 border-t border-gray-200">
                {{ $scans->links() }}
            </div>

        @else
            <p class="border border-red-400 p-4 my-4 flex">
                <svg class="w-5 h-5" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 115.4 122.88"><path d="M24.94,67.88A14.66,14.66,0,0,1,4.38,47L47.83,4.21a14.66,14.66,0,0,1,20.56,0L111,46.15A14.66,14.66,0,0,1,90.46,67.06l-18-17.69-.29,59.17c-.1,19.28-29.42,19-29.33-.25L43.14,50,24.94,67.88Z"/></svg>
                <span class="ml-4">
                {{ __('No results found, create a scan.') }}</span></p>
        @endif
    </x-layout.listwrapper>


    <div class="bg-light p-10 shadow-sm flex justify-end">
        <p class="text-2xl">
            <span>
            {{ __('Total:') }}
            </span>
            <strong class="text-green-400 text-4xl">
            {{ $total }}
            </strong>
        </p>
    </div>



<x-jet-dialog-modal wire:model="deleteItem" id="deleteItem">
    <x-slot name="title">
        {{ __('Delete scan') }}
    </x-slot>

    <x-slot name="content">
        <p>Are you sure want to delete?</p>
    </div>
    <div class="modal-footer pt-5">
    <button type="button" class="text-gray-500 mr-5" wire:click="closeModal()" >Cancel</button>
    <button type="button" class="text-red-500 " wire:click.prevent="delete()" class="btn btn-danger close-modal" data-dismiss="modal">Remove scan</button>
    </x-slot>

    <x-slot name="footer">

    </x-slot>
</x-jet-dialog-modal>

</x-backend-module-wrapper>
