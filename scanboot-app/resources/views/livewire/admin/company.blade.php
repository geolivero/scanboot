<x-backend-module-wrapper
    title="{{ __('Company') }}"
    >

        @if ($company && !is_array($company) && isset($company->id))
        <form class="p-4" wire:submit.prevent="update" action="{{ route('admin.companies.company', $company->id) }}" method="post">
        @else
        <form class="p-4" wire:submit.prevent="create" action="{{ route('admin.companies.company') }}" method="post">
        @endif
            @method('PUT')
            @csrf
            <x-layout.ui.anker
                url="{{ route('admin.companies') }}"
                label="{{ __('Go back') }}"
            />

            @if (session()->has('message'))
                <div>
                    <p class="text-green-400 py-2">
                        {{ session('message') }}
                    </p>
                </div>
            @endif

            <x-ui.inputs.select
                name="company.corporation_id"
                label="{{ __('Corporations') }}"
                :values="$corporations"
                wire:model.defer="company.corporation_id"
                selectedOption="{{ $company ?  $company->corporation_id : 0 }}"
                />

            <x-ui.inputs.input
                type="text"
                name="company.name"
                placeholder="{{ __('Company name') }}"
                label="{{ __('Name') }}"
                wire:model.defer="company.name"
                />


            <x-ui.inputs.input
                type="text"
                name="company.location"
                placeholder="{{ __('Location') }}"
                label="{{ __('Location') }}"
                wire:model.defer="company.location"
                />


            <x-ui.btn-holder>

                <div class="col-span-4 col-start-1 col-end-5">
                    @if ($company && !is_array($company) && isset($company->id))
                    <x-ui.delete-confirm-btn
                        id="{{ $company->id }}"
                        />
                    @endif
                </div>

                <div class="justify-items-end text-right">
                    <x-ui.inputs.btn
                        class=""
                        label="{{ __('Save') }}"
                        />
                </div>



            </x-ui.btn-holder>
        </form>
</x-backend-module-wrapper>
