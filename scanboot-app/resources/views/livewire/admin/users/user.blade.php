<x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
        @if ($user && isset($user->name))
        {{ $user->name }}
        @else
        {{ __('Add new user') }}
        @endif
    </h2>
</x-slot>

<div class="py-12">
    <div class="px-4">
        <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">

            @if ($user && !is_array($user) && isset($user->id))
            <form class="p-4" wire:submit.prevent="update" action="{{ route('admin.users.user', $user) }}" method="post">
            @else
            <form class="p-4" wire:submit.prevent="create" action="{{ route('admin.users.new') }}" method="post">
            @endif
                @method('PUT')
                @csrf
                <x-layout.ui.anker
                    url="{{ route('admin.users') }}"
                    label="{{ __('Go back') }}"
                />

                @if (session()->has('message'))
                    <div>
                        <p class="text-green-400 py-2">
                            {{ session('message') }}
                        </p>
                    </div>
                @endif



                <x-ui.inputs.input
                    type="text"
                    name="user.name"
                    placeholder="{{ __('Set your name') }}"
                    label="{{ __('Name') }}"
                    wire:model.defer="user.name"
                    />
                <x-ui.inputs.input
                    type="text"
                    name="user.email"
                    placeholder="{{ __('Set your email') }}"
                    label="{{ __('Email') }}"
                    wire:model.defer="user.email"
                    />

                <fieldset class="border-t border-gray-300 mt-2 py-2">
                    <legend>
                        {{ __('Corprorations') }}
                    </legend>

                <x-ui.inputs.multi-select
                    id="corporation_ids"
                    selectedLabel="selected_corporations"
                    :values="$corporations"
                    :selectedOptions="$selected_corporations"
                    />
                </fieldset>


                <x-ui.btn-holder>

                    <div class="col-span-4 col-start-1 col-end-5">
                        @if ($user && !is_array($user) && isset($user->id))
                        <x-ui.delete-confirm-btn
                            id="{{ $user->id }}"
                            />
                        @endif
                    </div>

                    <div class="justify-items-end text-right">
                        <x-ui.inputs.btn
                            class=""
                            label="{{ __('Save') }}"
                            />
                    </div>



                </x-ui.btn-holder>
            </form>

            <div class="py-4">
                <hr class="" />
            </div>

            @livewire('admin.roles', ['user' => $user ])
            <div class="py-4">
                <hr class="" />
            </div>
            @if($user)
            <div class="flex pt-4 pb-10 items-center justify-center">
                <a wire:click="sendWelcome" class="rounded-md bg-indigo-500 text-white px-4 py-2" href="#">
                    {{ __('Send welcome email') }}
                </a>
            </div>
            @endif

        </div>
    </div>
</div>
