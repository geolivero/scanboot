<x-backend-module-wrapper
    title="{{ __('Tag') }}"
    >

        @if ($tag && !is_array($tag) && isset($tag->id))
        <form class="p-4" wire:submit.prevent="update" action="{{ route('admin.tags.tag', $tag->id) }}" method="post">
        @else
        <form class="p-4" wire:submit.prevent="create" action="{{ route('admin.tags.tag') }}" method="post">
        @endif
            @method('PUT')
            @csrf
            <x-layout.ui.anker
                url="{{ route('admin.tags') }}"
                label="{{ __('Go back') }}"
            />

            @if (session()->has('message'))
                <div>
                    <p class="text-green-400 py-2">
                        {{ session('message') }}
                    </p>
                </div>
            @endif

            <x-ui.inputs.input
                type="text"
                name="tag.name"
                placeholder="{{ __('tag name') }}"
                label="{{ __('Name') }}"
                wire:model.defer="tag.name"
                />


            <x-ui.btn-holder>

                <div class="col-span-4 col-start-1 col-end-5">
                    @if ($tag && !is_array($tag) && isset($tag->id))
                    <x-ui.delete-confirm-btn
                        id="{{ $tag->id }}"
                        />
                    @endif
                </div>

                <div class="justify-items-end text-right">
                    <x-ui.inputs.btn
                        class=""
                        label="{{ __('Save') }}"
                        />
                </div>



            </x-ui.btn-holder>
        </form>
</x-backend-module-wrapper>
