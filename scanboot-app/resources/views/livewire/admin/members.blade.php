<x-backend-module-wrapper
    title="{{ __('Members') }}"
    >
    @if(!empty($members))
    <div class="p-4 border-b border-gray-300">
        {{-- <x-ui.btn url="#" label="{{ __('Download XLS') }}" /> --}}
        <button
            wire:click="export('csv')"
            class="bg-blue-500 flex items-center py-2 text-white font-bold uppercase text-xs px-4 rounded outline-none focus:outline-black hover:bg-blue-700">
            <svg class="fill-current mr-2 text-white w-4 h-4" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="92" height="92" viewBox="0 0 92 92" enable-background="new 0 0 92 92"><path id="XMLID_1335_" d="M89 58.8V86c0 2.8-2.2 5-5 5H8c-2.8.0-5-2.2-5-5V58.8c0-2.8 2.2-5 5-5s5 2.2 5 5V81h66V58.8c0-2.8 2.2-5 5-5S89 56 89 58.8zM42.4 65c.9 1 2.2 1.5 3.6 1.5s2.6-.5 3.6-1.5l19.9-20.4c1.9-2 1.9-5.1-.1-7.1-2-1.9-5.1-1.9-7.1.1L51 49.3V6c0-2.8-2.2-5-5-5s-5 2.2-5 5v43.3L29.6 37.7c-1.9-2-5.1-2-7.1-.1-2 1.9-2 5.1-.1 7.1L42.4 65z"
                /></svg> Download XLS
        </button>
    </div>
    @endempty
    <div class="p-4 border-b border-gray-300 md:grid md:grid-cols-2 gap-5">
        <div>
            <x-ui.btn
                label="{{ __('Create a member') }}"
                url="{{ route('admin.members.member') }}"
                />
        </div>
    </div>


    @if($members && count($members) > 0)
    <div wire:ignore class="pt-4 px-4 grid grid-cols-3 gap-2">
        <div>
            <label class="pb-2 block" for="tags">{{ __('Companies') }}</label>
            <x-ui.inputs.multi-select
                id="companies"
                selectedLabel="searched_companies"
                :values="$companies"
                :selectedOptions="[]"
                />
        </div>
        <div>
            <label class="pb-2 block" for="tags">{{ __('Tags') }}</label>
            <x-ui.inputs.multi-select
                id="tags"
                selectedLabel="searched_tags"
                :values="$tags"
                :selectedOptions="[]"
                />
        </div>
        <div>
            <label class="pb-2 block" for="members">{{ __('Members') }}</label>
            <x-ui.inputs.multi-select
                id="members"
                selectedLabel="selected_members"
                :values="$allmembers"
                :selectedOptions="[]"
                />
        </div>
    </div>
    @endif
    <div class="bg-light p-10 shadow-sm flex justify-end">
        <p class="text-2xl">
            <span>
            {{ __('Total:') }}
            </span>
            <strong class="text-green-400 text-4xl">
            {{ $total }}
            </strong>
        </p>
    </div>
    <x-layout.listwrapper>
        @if($members)
            @forelse ($members as $member)
                <x-layout.row-item
                    label="{{ $member->name }}"
                    url="{{ route('admin.members.member.edit', $member) }}"
                    date="{{ date('m/d/Y H:i', strtotime($member->created_at)) }}"
                    id="{{ $member->id }}"
                    last="{{ $loop->last }}"
                    >
                    <div>
                        CODE: <strong class="bg-black inline-block text-white p-2 font-serif font-light tracking-widest">{{ $member->user_hash }}</strong>
                    </div>
                </x-layout.row-item>
            @empty
                <p class="border border-red-400 p-4 my-4">{{ __('No results found') }}</p>
            @endforelse

            <div class="py-4 mt-10 border-t border-gray-200">
                {{ $members->links() }}
            </div>

            <div class="bg-light p-10 shadow-sm flex justify-end">
                <p class="text-2xl">
                    <span>
                    {{ __('Total:') }}
                    </span>
                    <strong class="text-green-400 text-4xl">
                    {{ $total }}
                    </strong>
                </p>
            </div>
        @else
            <p class="border border-red-400 p-4 my-4 flex">
                <svg class="w-5 h-5" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 115.4 122.88"><path d="M24.94,67.88A14.66,14.66,0,0,1,4.38,47L47.83,4.21a14.66,14.66,0,0,1,20.56,0L111,46.15A14.66,14.66,0,0,1,90.46,67.06l-18-17.69-.29,59.17c-.1,19.28-29.42,19-29.33-.25L43.14,50,24.94,67.88Z"/></svg>
                <span class="ml-4">
                {{ __('No results found, create a member.') }}</span></p>
        @endif
    </x-layout.listwrapper>


</x-backend-module-wrapper>
