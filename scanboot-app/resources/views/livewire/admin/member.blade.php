<x-backend-module-wrapper
    title="{{ __('member') }}"
    >

        @if ($member && !is_array($member) && isset($member->id))
        <form class="p-4" wire:submit.prevent="update" action="{{ route('admin.members.member', $member->id) }}" method="post">
        @else
        <form class="p-4" wire:submit.prevent="create" action="{{ route('admin.members.member') }}" method="post">
        @endif
            @method('PUT')
            @csrf
            <x-layout.ui.anker
                url="{{ route('admin.members') }}"
                label="{{ __('Go back') }}"
            />

            @if (session()->has('message'))
                <div>
                    <p class="text-green-400 py-2">
                        {{ session('message') }}
                    </p>
                </div>
            @endif
            @if (isset($member->user_hash))
            <div class="pb-4">
                <p class="py-2">
                    {{ __('Personal members code, please write this to the NFC chip') }}
                </p>
                <div class="inline-flex items-center">
                    <strong class="bg-black inline-block text-white p-2 mr-2 font-serif font-light tracking-widest">{{ $member->user_hash }}</strong>
                    <a wire:click="updateNFC" href="#">
                        <svg class="w-5 h-5" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="438.529" height="438.528" viewBox="0 0 438.529 438.528" style="enable-background:new 0 0 438.529 438.528"><g><g><path d="M433.109 23.694c-3.614-3.612-7.898-5.424-12.848-5.424-4.948.0-9.226 1.812-12.847 5.424l-37.113 36.835c-20.365-19.226-43.684-34.123-69.948-44.684C274.091 5.283 247.056.003 219.266.003c-52.344.0-98.022 15.843-137.042 47.536C43.203 79.228 17.509 120.574 5.137 171.587v1.997c0 2.474.903 4.617 2.712 6.423 1.809 1.809 3.949 2.712 6.423 2.712h56.814c4.189.0 7.042-2.19 8.566-6.565 7.993-19.032 13.035-30.166 15.131-33.403 13.322-21.698 31.023-38.734 53.103-51.106 22.082-12.371 45.873-18.559 71.376-18.559 38.261.0 71.473 13.039 99.645 39.115l-39.406 39.397c-3.607 3.617-5.421 7.902-5.421 12.851.0 4.948 1.813 9.231 5.421 12.847 3.621 3.617 7.905 5.424 12.854 5.424h127.906c4.949.0 9.233-1.807 12.848-5.424 3.613-3.616 5.42-7.898 5.42-12.847V36.542C438.529 31.593 436.733 27.312 433.109 23.694z"/><path d="M422.253 255.813h-54.816c-4.188.0-7.043 2.187-8.562 6.566-7.99 19.034-13.038 30.163-15.129 33.4-13.326 21.693-31.028 38.735-53.102 51.106-22.083 12.375-45.874 18.556-71.378 18.556-18.461.0-36.259-3.423-53.387-10.273-17.13-6.858-32.454-16.567-45.966-29.13l39.115-39.112c3.615-3.613 5.424-7.901 5.424-12.847.0-4.948-1.809-9.236-5.424-12.847-3.617-3.62-7.898-5.431-12.847-5.431H18.274c-4.952.0-9.235 1.811-12.851 5.431C1.807 264.844.0 269.132.0 274.08v127.907c0 4.945 1.807 9.232 5.424 12.847 3.619 3.61 7.902 5.428 12.851 5.428 4.948.0 9.229-1.817 12.847-5.428l36.829-36.833c20.367 19.41 43.542 34.355 69.523 44.823 25.981 10.472 52.866 15.701 80.653 15.701 52.155.0 97.643-15.845 136.471-47.534 38.828-31.688 64.333-73.042 76.52-124.05.191-.38.281-1.047.281-1.995.0-2.478-.907-4.612-2.715-6.427C426.874 256.72 424.731 255.813 422.253 255.813z"/></g></g><g/><g/><g/><g/><g/><g/><g/><g/><g/><g/><g/><g/><g/><g/><g/></svg>
                    </a>
                </div>

            </div>
            @endif

            <x-ui.inputs.select
                name="member.company_id"
                label="{{ __('Company') }}"
                :values="companies()->get"
                wire:model.defer="member.company_id"
                selectedOption="{{ isset($member->company_id) ?  $member->company_id : 0 }}"
                />
            <x-ui.inputs.select
                name="member.credits_categories_id"
                label="{{ __('Asign credits package') }}"
                :values="$creditpackages"
                wire:model.defer="member.credits_categories_id"
                selectedOption="{{ isset($member->credits_categories_id) ?  $member->credits_categories_id : 0 }}"
                />

            <div wire:ignore>
                <label class="block pb-2" for="tags">{{ __('Create or choose tags') }}</label>
                <select id="tags" autocomplete="off" placeholder="{{ __('Choose an option') }}" multiple class="multi_select">
                <option value="0">{{ __('Choose a value') }}</option>
                @forelse($tags as $tag)
                    <option @if(array_search($tag, $selectedTags) > -1)  selected @endif value="{{ $tag }}">
                        {{ $tag }}
                    </option>
                @empty
                    <option value="0">{{ __('No options yet, create one!') }}</option>
                @endforelse
                </select>
            </div>

            <script>
                new TomSelect('#tags', {
                    create: true,
                    onChange: function (value) {
                        @this.set('tags', value);
                    },
                    onItemRemove: function (value) {
                        console.log(value);
                    }
                });
            </script>

            <x-ui.inputs.input
                type="text"
                name="member.name"
                placeholder="{{ __('member name') }}"
                label="{{ __('Name') }}"
                wire:model.defer="member.name"
                />




            <x-ui.btn-holder>

                <div class="col-span-4 col-start-1 col-end-5">
                    @if (isset($member->id))
                    <x-ui.delete-confirm-btn
                        id="{{ $member->id }}"
                        />
                    @endif
                </div>

                <div class="justify-items-end text-right">
                    <x-ui.inputs.btn
                        class=""
                        label="{{ __('Save') }}"
                        />
                </div>
            </x-ui.btn-holder>
        </form>
</x-backend-module-wrapper>
