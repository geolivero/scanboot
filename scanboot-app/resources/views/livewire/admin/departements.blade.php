<x-backend-module-wrapper
    title="{{ __('Departement') }}"
    >
    <div class="p-4 border-b border-gray-300 md:grid md:grid-cols-2 gap-5">
        <div>
            <x-ui.btn
                label="{{ __('Create a departement') }}"
                url="{{ route('admin.departements.departement') }}"
                />
        </div>
    </div>
    @if($departements && count($departements) > 0)
    <x-ui.filters
        :filters="$filter_list"/>
    @endif

    <x-layout.listwrapper>
        @if($departements)
            @forelse ($departements as $departement)
                <x-layout.row-item
                    label="{{ $departement->name }}"
                    url="{{ route('admin.departements.departement.edit', $departement) }}"
                    date="{{ date('m/d/Y H:i', strtotime($departement->updated_at)) }}"
                    id="{{ $departement->id }}"
                    last="{{ $loop->last }}"
                    >
                    <div>
                        Comp. {{ $departement->company->name }}
                    </div>
                </x-layout.row-item>
            @empty
                <p class="border border-red-400 p-4 my-4">{{ __('No results found') }}</p>
            @endforelse
        @else
            <p class="border border-red-400 p-4 my-4 flex">
                <svg class="w-5 h-5" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 115.4 122.88"><path d="M24.94,67.88A14.66,14.66,0,0,1,4.38,47L47.83,4.21a14.66,14.66,0,0,1,20.56,0L111,46.15A14.66,14.66,0,0,1,90.46,67.06l-18-17.69-.29,59.17c-.1,19.28-29.42,19-29.33-.25L43.14,50,24.94,67.88Z"/></svg>
                <span class="ml-4">
                {{ __('No results found, create a departement.') }}</span></p>
        @endif
    </x-layout.listwrapper>

</x-backend-module-wrapper>
