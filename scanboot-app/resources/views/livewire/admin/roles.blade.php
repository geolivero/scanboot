

@if ($user && !is_array($user) && isset($user->id))
<form class="p-4" wire:submit.prevent="update" action="{{ route('admin.users.user', $user) }}" method="post">
@else
<form class="p-4" wire:submit.prevent="create" action="{{ route('admin.users.new') }}" method="post">
@endif
    @method('PUT')
    @csrf
    @if (session()->has('message'))
        <div>
            <p class="text-green-400 py-2">
                {{ session('message') }}
            </p>
        </div>
    @endif


    <x-ui.inputs.checkboxes
        label="{{ __('Roles') }}"
        name="user.user_roles"
        model="user_roles"
        :rows="$roles"
        />
    <div class="justify-items-end text-right">
        <x-ui.inputs.btn
            class=""
            label="{{ __('Save') }}"
            />
    </div>
</form>
