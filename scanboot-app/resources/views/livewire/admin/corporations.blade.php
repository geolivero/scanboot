<x-backend-module-wrapper
    title="{{ __('Corporation') }}"
    >
    <div class="p-4">
        <x-ui.btn
            label="{{ __('Create corporation') }}"
            url="{{ route('admin.corporations.corporation') }}"
            />
    </div>
    <x-ui.filters
        :filters="$filter_list"/>
    <x-layout.listwrapper>
        @forelse ($corporations as $corporation)
            <x-layout.row-item
                label="{{ $corporation->name }}"
                url="{{ route('admin.corporations.corporation.edit', $corporation) }}"
                date="{{ $corporation->created_at->format('m/d/Y H:i') }}"
                id="{{ $corporation->id }}"
                last="{{ $loop->last }}"
                >
                {{ $corporation->email }}
            </x-layout.row-item>
        @empty
            <p>{{ __('auth.no_results') }}</p>
        @endforelse
    </x-layout.listwrapper>
    <div class="p-4">
        {{ $corporations->links() }}
    </div>
</x-backend-module-wrapper>
