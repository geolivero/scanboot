<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Users ' . $type) }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="px-4">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <x-layout.listwrapper>
                    <form action="{{ route('users.' . $type, $user ?? false) }}" method="post">
                        @method('PUT')
                        @csrf
                        {{ $type }}
                        <x-ui.inputs.input
                            type="text"
                            name="name"
                            placeholder="{{ __('Set your name') }}"
                            label="{{ __('Name') }}"
                            wire:model="user.name"
                            />
                        <x-ui.inputs.input
                            type="text"
                            name="email"
                            placeholder="{{ __('Set your email') }}"
                            label="{{ __('Email') }}"
                            wire:model="user.email"
                            />

                        <x-ui.btn-holder>
                                <x-ui.inputs.btn
                                    class="col-span-3"
                                    label="{{ __('Save') }}"
                                    />
                        </x-ui.btn-holder>
                    </form>
                </x-layout.listwrapper>
            </div>
        </div>
    </div>
</x-app-layout>
