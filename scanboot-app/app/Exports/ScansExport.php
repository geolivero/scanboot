<?php

namespace App\Exports;


use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class ScansExport implements FromCollection, WithHeadings, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public $collection;


    public function __construct($data)
    {
        $this->collection = $data;
    }

    public function headings():array
    {
        return [
            __('Name'),
            __('Credits'),
            __('Total credits used'),
            __('Company'),
            __('Tags'),
            __('Date time')
        ];
    }

    public function map($scan):array
    {
        $tags = $scan->usersCredits->tags;

        $tag_review = [];
        if ($tags) {
            foreach($tags as $tag) {
                $tag_review[] = $tag->tag->name;
            }
        }
        return [
            $scan->usersCredits->name,
            $scan->usersCredits->totalCredits(),
            $scan->usersCredits->usedCredits(date('Y-m-01', strtotime($scan->created_at)),$scan->created_at),
            $scan->company->name,
            'tags' => implode(',', $tag_review),
            date('m/d/Y g:i A', strtotime($scan->created_at))
        ];
    }
    public function collection()
    {
        //
        return $this->collection;
    }
}
