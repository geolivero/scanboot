<?php

namespace App\Exports;


use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class MembersExport implements FromCollection, WithHeadings, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public $collection;


    public function __construct($data)
    {
        $this->collection = $data;
    }

    public function headings():array
    {
        return [
            __('Name'),
            __('Company'),
            __('Tags'),
            __('Date time')
        ];
    }

    public function map($scan):array
    {
        $tags = $scan->tags;

        $tag_review = [];
        if ($tags) {
            foreach($tags as $tag) {
                $tag_review[] = $tag->tag->name;
            }
        }
        return [
            $scan->name,
            $scan->company->name,
            'tags' => implode(',', $tag_review),
            date('m/d/Y g:i A', strtotime($scan->created_at))
        ];
    }
    public function collection()
    {
        //
        return $this->collection;
    }
}
