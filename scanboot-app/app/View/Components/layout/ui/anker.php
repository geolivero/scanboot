<?php

namespace App\View\Components\layout\ui;

use Illuminate\View\Component;

class anker extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $url;
    public $label;

    public function __construct($url, $label)
    {
        //
        $this->url = $url;
        $this->label = $label;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.layout.ui.anker');
    }
}
