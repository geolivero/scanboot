<?php

namespace App\View\Components\layout;

use Illuminate\View\Component;

class row-item extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */

    public $label;
    public $url;
    public $date;
    public $last;
    public $id;

    public function __construct($label, $url, $date, $last, $id)
    {
        //
        dd($url);
        $this->label = $label;
        $this->url = $url;
        $this->date = $date;
        $this->last = $last;
        $this->id = $id;

    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.layout.row-item');
    }
}
