<?php

namespace App\View\Components\Ui;

use Illuminate\View\Component;

class Filters extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    var $filters;

    public function __construct($filters)
    {
        //
        $this->filters = $filters;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.ui.filters');
    }
}
