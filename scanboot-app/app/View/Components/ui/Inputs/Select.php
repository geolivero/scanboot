<?php

namespace App\View\Components\Ui\Inputs;

use Illuminate\View\Component;

class Select extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $selectedOption = 0;
    public $values = [];
    public $name = '';
    public $label = '';

    public function __construct($name, $label, $values, $selectedOption)
    {
        $this->values = $values;
        $this->selectedOption = $selectedOption;
        $this->name = $name;
        $this->label = $label;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.ui.inputs.select');
    }
}
