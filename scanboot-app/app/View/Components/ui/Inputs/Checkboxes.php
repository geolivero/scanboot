<?php

namespace App\View\Components\Ui\Inputs;

use Illuminate\View\Component;

class Checkboxes extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $name;
    public $label;
    public $rows;
    public $model;

    public function __construct($name, $label, $rows, $model)
    {
        //
        // dd($rows);
        // $this->rows = $rows;
        $this->name = $name;
        $this->label = $label;
        $this->rows = $rows;
        $this->model = $model;

    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.ui.inputs.checkboxes');
    }
}
