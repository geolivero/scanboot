<?php

namespace App\View\Components\Ui\Inputs;

use Illuminate\View\Component;

class MultiSelect extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $id;
    public $selectedOptions = [];
    public $selectedLabel = '';
    public $values = [];
    public $identifier = '';

    public function __construct($id, $values, $selectedLabel, $selectedOptions, $identifier = 'id')
    {
        //
        $this->id = $id;
        $this->values = $values;
        $this->selectedLabel = $selectedLabel;
        $this->selectedOptions = $selectedOptions;
        $this->identifier = $identifier;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.ui.inputs.multi-select');
    }
}
