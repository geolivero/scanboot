<?php

namespace App\View\Components\Ui;

use Illuminate\View\Component;

class Pikaday extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    var $id;
    var $label;

    public function __construct($label, $id)
    {
        //
        $this->label = $label;
        $this->id = $id;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.ui.pikaday');
    }
}
