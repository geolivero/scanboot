<?php

namespace App\Models;

use App\Models\Admin\Devices;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class DashboardToken extends Model
{
    use HasFactory;

    protected $fillable = [
        'token',
        'corporations_id',
        'devices_id'
    ];

    public function device()
    {
        return $this->belongsTo(Devices::class, 'devices_id');
    }
}
