<?php

namespace App\Models;

use Companies;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Credits extends Model
{
    use HasFactory;

    //protected $dateFormat = 'Y-m-d';
    protected $garded = [
        'updated_at'
    ];

    protected $fillable = [
        'users_credits_id', 'company_id', 'device_id'
    ];

    public function usersCredits () {
        return $this->belongsTo(UsersCredits::class);
    }

    public function company () {
        return $this->belongsTo(Admin\Companies::class);
    }

    // public function getCreatedAtAttribute($value) {
    //     setlocale(LC_TIME, currtimezone());
    //     $date = $value;
    //     date_timezone_set($date , timezone_open(currtimezone()));
    //     $date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value, 'UTC -4')->setTimezone(currtimezone());
    //     return $date->format('m/d/Y g:i A');
    // }

    // protected function asDateTime($value)
    // {
    //     return parent::asDateTime($value)->format('m/d/Y g:i A');
    // }


}
