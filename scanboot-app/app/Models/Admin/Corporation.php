<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Corporation extends Model
{
    use HasFactory;

    protected $table = 'corporations';
    protected $primaryKey = 'id';

    protected $fillable = [
        'name'
    ];


    public function user_corporation ()
    {
        return $this->hasMany(UserCorporation::class);
    }

    public function companies() {
        return $this->hasMany(Companies::class);
    }
}
