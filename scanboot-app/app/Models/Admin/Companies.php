<?php

namespace App\Models\Admin;

use Credits;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Companies extends Model
{
    use HasFactory;

    protected $table = 'companies';
    protected $primaryKey = 'id';

    protected $fillable = [
        'name', 'location', 'corporation_id'
    ];

    public function corporation () {
        return $this->belongsTo('App\Models\Admin\Corporation');
    }

    public function user_corporation () {
        return $this->belongsTo(UserCorporation::class);
    }

    public function credits () {
        return $this->hasMany(Credits::class);
    }
}
