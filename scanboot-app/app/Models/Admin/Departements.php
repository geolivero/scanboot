<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Departements extends Model
{
    use HasFactory;

    protected $table = 'departements';
    protected $primaryKey = 'id';

    protected $fillable = [
        'name', 'company_id'
    ];

    public function company () {
        return $this->belongsTo(Companies::class);
    }
}
