<?php

namespace App\Models\Admin;

use App\Models\UsersCredits;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MemberTags extends Model
{
    use HasFactory;

    protected $fillable = [
        'tags_id', 'users_credits_id', 'companies_id'
    ];


    protected $table = 'member_tags';


    public function tag() {
        return $this->belongsTo(Tags::class, 'tags_id');
    }

    public function usersCredits () {
        return $this->belongsTo(UsersCredits::class, 'users_credits_id');
    }
}
