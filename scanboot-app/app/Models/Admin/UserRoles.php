<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserRoles extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'roles_id', 'user_id'
    ];


    public function user () {
        return $this->belongsTo('App\Models\User');
    }
    public function roles () {
        return $this->belongsTo('App\Models\Admin\Roles');
    }

}
