<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserCorporation extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $table = 'user_corporations';
    protected $primaryKey = 'id';

    protected $fillable = [
        'corporation_id', 'user_id'
    ];


    public function user () {
        return $this->belongsTo('App\Models\User');
    }


    public function corporation () {
        return $this->belongsTo(Corporation::class);
    }

}
