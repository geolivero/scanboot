<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CreditsCategories extends Model
{
    use HasFactory;
    protected $fillable = [
        'name', 'corporations_id', 'credits'
    ];
}
