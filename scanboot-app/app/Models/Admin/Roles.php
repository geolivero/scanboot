<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Roles extends Model
{
    use HasFactory;

    //
    protected $fillable = [
        'name'
    ];


    public function user_roles ()
    {
        return $this->hasMany('App\Models\Admin\UserRoles');
    }

}
