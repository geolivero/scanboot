<?php

namespace App\Models\Admin;

use App\Models\DashboardToken;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Devices extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'corporations_id'
    ];


    function token()
    {
        return $this->hasOne(DashboardToken::class);
    }

}
