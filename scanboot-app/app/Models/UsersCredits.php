<?php

namespace App\Models;

use App\Models\Admin\MemberTags;
use App\Models\Admin\Tags;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UsersCredits extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'user_id', 'user_picture', 'user_hash', 'credits_categories_id', 'company_id'
    ];

    public function creditCategorie () {
        return $this->belongsTo(CreditsCategories::class, 'credits_categories_id');
    }

    public function credits () {
        return $this->hasMany(Credits::class, 'users_credits_id');
    }

    public function company () {
        return $this->belongsTo(Admin\Companies::class);
    }

    public function totalCredits () {
        if ($this->creditCategorie) {
            return $this->creditCategorie->credits;
        }
        return false;
    }

    public function usedCredits ($month, $date) {
        $currentCredits = $this->totalCredits();
        $credits = Credits::whereBetween('created_at',[$month, $date])->where('users_credits_id', $this->id)->count();
        $total_credits = $currentCredits - $credits;
        return $total_credits;
    }

    public function tags () {
        return $this->hasMany(MemberTags::class, 'users_credits_id');
    }

}
