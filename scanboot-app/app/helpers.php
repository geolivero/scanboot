<?php

if (!function_exists('corps')) {
    function corps () {
        return \Corp::get();
    }
}

if (!function_exists('currcorp')) {
    function currcorp () {
        return \Corp::getCurrentCorp();
    }
}

if (!function_exists('companies')) {
    function companies () {
        return (object)[
            'get' => \Corp::companies(),
            'ids' => \Corp::company_ids()
        ];
    }
}

if (!function_exists('currtimezone')) {
    function currtimezone () {
        $corp = currcorp();
        return isset($corp->timezone) ? $corp->timezone : 'UTC';
    }
}

if (!function_exists('datezone')) {
    function datezone($date) {
        //dd($date);
        return $date->timezone(currtimezone())->format('m/d/Y g:i A');
    }
}
