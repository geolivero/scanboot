<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Support\HtmlString;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class WelcomeMessage extends Notification
{
    use Queueable;
    public $user;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        //
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {

        $token = app('auth.password.broker')->createToken($this->user);
        $reset = url(route('password.reset', ['token' => $token, 'email' => $this->user->email], false));

        return (new MailMessage)
            ->subject('Your account has been created at Scanboot')
            ->greeting('Hello '. ucfirst($this->user->name) . '!')
            ->line(new HtmlString('Welcome to <strong>Scanboot</strong>, your access to "control"!<br/>'))
            ->line('To get started please click on the button below.')
            ->line('Enter your fresh password then you will need to login with your e-mail and password after to that you will be redirected to your personal space.')
            ->action('Get started', $reset)
            ->line(new HtmlString('After login in, please activate your <strong>Two Factor Authentication<strong>.'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
