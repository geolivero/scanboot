<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App;

class CorporationProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        App::bind('Corporation',function() {
            return new App\Services\CorporationService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
