<?php

namespace App\Providers;

use Illuminate\Database\Query\Builder;
use Illuminate\Support\ServiceProvider;

class ModelHelpersProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Builder::macro('search', function ($field, $string) {
            return $string ? $this->orWhere($field, 'like', '%' . $string  . '%' ) : $this;
        });

        Builder::macro('sort', function ($field, $asc) {
            $type = $asc ? 'ASC': 'DESC';
            return $field ? $this->orderBy($field, $type) : $this;
        });
    }
}
