<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App;

class AdminProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        // $this->app->bind('Admin', function () {
        //     return 'Halo';
        // });

        App::bind('Admin',function() {
            return new App\Services\AdminService();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
