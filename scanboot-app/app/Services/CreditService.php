<?php
namespace App\Services;

use App\Models\Credits;
use App\Models\Admin\Devices;
use App\Models\Admin\Companies;
use Illuminate\Support\Facades\Log;

class CreditService {

    static public function getUserCredits ($user)
    {
        $currentCredits = $user->totalCredits();
       $month = date('Y-m-01');
        $lastday = date('Y-m-t');
        $credits = Credits::where('created_at', '>', $month)
            ->where('created_at', '<', $lastday)
            ->where('users_credits_id', $user->id)
            ->count();
        $total_credits = $currentCredits - $credits;
        return $total_credits;
    }

    static public function getUserCreditsTotalByDate($user, $date)
    {
        $currentCredits = $user->totalCredits();
        $month = date('Y-m-01');
        $credits = Credits::whereBetween('created_at',[$month, $date])->where('users_credits_id', $user->id)->count();
        $total_credits = $currentCredits - $credits;
        return $total_credits;
    }

    static public function getScanResultsByCorps ($id, $device_id = 0)
    {
        $company_results = Companies::where('corporation_id', $id)->get();
        $companies = [];
        $device = '';
        foreach($company_results as $company) {
            $companies[] = $company->id;
        }
        if ($device_id) {
            $credits = Credits::orderBy('created_at', 'DESC')->where('device_id', $device_id)->whereIn('company_id', $companies)->limit(5)->get();
            $latest =  Credits::orderBy('created_at', 'DESC')->where('device_id', $device_id)->whereIn('company_id', $companies)->first();
            $curdevice = Devices::findOrFail($device_id);
            $device = $curdevice->name;
        } else {
            $credits = Credits::orderBy('created_at', 'DESC')->whereIn('company_id', $companies)->limit(5)->get();
            $latest =  Credits::orderBy('created_at', 'DESC')->whereIn('company_id', $companies)->first();
        }


        $latestCredits = [];
        if (!$latest) {
            return false;
        }

        $user = $latest->usersCredits;
        $current_user = [
            'name' => $user->name,
            'credits' => self::getUserCredits($user)
        ];

        Log::channel('daily')->debug('Current user logged: ' . $user->name);

        //setlocale(LC_TIME, currtimezone());



        foreach($credits as $credit) {
            //$date = new DateTime($credit->created_at, new DateTimeZone(currtimezone());
            $date = $credit->updated_at;
            //date_timezone_set($date , timezone_open(currtimezone()));
            $user = $credit->usersCredits;
            $latestCredits[] = [
                'name' => $user->name,
                'total' => self::getUserCreditsTotalByDate($user, $credit->updated_at),
                //'date' => $credit->created_at,
                //'date' => date('m/d/Y g:i A', $date),
                'date' => $credit->updated_at->format('m/d/Y g:i A'),
                'id' => $credit->id
            ];
        }

        return [
            'current' => $current_user,
            'credits' => $latestCredits,
            'device' => $device
        ];
    }
}
