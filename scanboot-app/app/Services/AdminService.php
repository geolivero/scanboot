<?php
namespace App\Services;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class AdminService {
    public function show () {
        return Auth::user()->name;
    }

    public function user_roles() {

        $values = Cache::rememberForever('user_roles', function () {
            $names = [];
            if (Auth::user()->user_roles) {
                $roles = Auth::user()->user_roles->where('user_id', Auth::user()->id);

                $names = [];

                if (!empty($roles)) {
                    foreach($roles as $role) {
                        if ($role->roles) {
                            $saved_role = $role->roles->where('id', $role->roles_id)->first();
                            $names[] = $saved_role->name;
                        }
                    }
                }
            }
            return $names;
        });



        return $values;
    }

    public function isRole ($role_name = '') {
        return in_array($role_name, $this->user_roles());
    }
}
