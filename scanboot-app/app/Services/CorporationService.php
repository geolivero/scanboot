<?php
namespace App\Services;

use App\Models\Admin\UserCorporation;
use Illuminate\Support\Facades\Cache;

class CorporationService {

    public function get()
    {
        $values = Cache::rememberForever('corporations', function () {
            $user = auth()->user();
            $corps = [];
            if ($user->user_corporations) {
                foreach($user->user_corporations as $corp) {
                    $corps[] = $corp->corporation;
                }
            }

            return $corps;
        });
        return $values;
    }

    public function getCurrentCorp()
    {
        $values = Cache::rememberForever('corporations', function () {
            $user = auth()->user();
            $corps = [];
            if (isset($user->user_corporations)) {
                foreach($user->user_corporations as $corp) {
                    $corps[] = $corp->corporation;
                }
            }

            return isset($corps[0]) ? $corps[0] : (object)[];
        });
        return $values;
    }

    public function companies()
    {

        $values = Cache::rememberForever('companies', function () {
            $user = auth()->user();
            $companies = [];
            foreach($user->user_corporations as $corp) {
                $corpcompanies = $corp->corporation->companies;
                foreach($corpcompanies as $company) {
                    $companies[] = $company;
                }
            }
            return $companies;
        });
        return $values;
    }

    public function company_ids ()
    {
        $companies = $this->companies();
        $ids = [];
        if ($companies) {
            foreach($companies as $company) {
                $ids[] = $company->id;
            }
        }

        return $ids;
    }
}
