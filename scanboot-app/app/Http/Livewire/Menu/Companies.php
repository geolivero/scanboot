<?php

namespace App\Http\Livewire\Menu;

use App\Models\Admin\UserCorporation;
use Livewire\Component;
use Illuminate\Support\Facades\Auth;


class Companies extends Component
{

    public function render()
    {
        $user = Auth::user();
        $companies = UserCorporation::where('user_id', $user->id)->get();

        return view('livewire.admin.companies
        ', [
            'companies' => $companies
        ]);
    }
}
