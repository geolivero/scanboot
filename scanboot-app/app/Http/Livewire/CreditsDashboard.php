<?php

namespace App\Http\Livewire;

use App\Models\DashboardToken;
use Livewire\Component;
use Illuminate\Support\Facades\Session;

class CreditsDashboard extends Component
{

    public $token;

    protected $rules = [
        'token' => 'required'
    ];


    public function mount ()
    {
        $this->token = '';
    }

    public function access()
    {
        $values = $this->validate();
        $found = DashboardToken::where('token', $values['token'])->first();
        if ($found) {
            session()->put('dashboard_token', $found->token);
            return redirect()->to(route('credits.dashboard.view'));
        } else {
            session()->flash('error', __('Token is not correct'));
        }
    }
    public function render()
    {
        return view('livewire.credits-dashboard')->layout('layouts.plain');
    }
}
