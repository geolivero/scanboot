<?php

namespace App\Http\Livewire\Admin;

use App\Models\Admin\CreditsCategories;
use App\Models\Admin\MemberTags;
use Livewire\Component;
use App\Models\Admin\Tags;
use Illuminate\Support\Str;
use App\Models\UsersCredits;


class Member extends Component
{
    public $member;
    public $user;
    public $tags;
    public $creditpackages;
    public $selectedTags;
    public $corp;

    protected $rules = [
        'member.name' => 'required|string|min:2',
        'member.company_id' => 'required|integer',
        'member.credits_categories_id' => 'required|integer',
        'tags' => 'required'
    ];

    public function confirmRemoval()
    {
        $this->member->tags()->delete();
        $this->member->delete();
        return redirect()->to(route('admin.members'));
    }

    public function updateNFC()
    {
        $this->member->user_hash = $this->member->id . strtolower(Str::random(4));
        $this->member->save();

        session()->flash('message', __('NFC refreshed, please rewrite the NFC chip'));
    }

    private function updateTags ($tags, $member)
    {
        if ($tags) {
            $toDelete = [];

            foreach($this->selectedTags as $selected) {
                if (!in_array($selected, $tags)) {
                    $toDelete[] = $selected;
                }
            }

            foreach($toDelete as $del) {
                $tag = Tags::where('name', $del)->first();

                MemberTags::where('tags_id',$tag->id)
                    ->where('users_credits_id', $member->id)
                    ->where('companies_id', $member->company_id)
                    ->delete();
            }
            foreach($tags as $tag) {
                $savedtags = Tags::where('name', strtolower($tag))->get();

                if ($savedtags->isEmpty()) {
                    $newTag = Tags::create([
                        'name' => strtolower($tag),
                        'corporations_id' => $this->corp->id
                    ]);
                    MemberTags::create([
                        'tags_id' => $newTag->id,
                        'users_credits_id' => $member->id,
                        'companies_id' => $member->company_id
                    ]);
                } else {

                    foreach($savedtags as $tag) {
                        $tagExists = MemberTags::where  ('users_credits_id', $member->id)
                        ->where('tags_id', $tag->id)
                        ->where('companies_id', $member->company_id)->get();
                        if ($tagExists->isEmpty()) {
                            MemberTags::create([
                                'tags_id' => $tag->id,
                                'users_credits_id' => $member->id,
                                'companies_id' => $member->company_id
                            ]);
                        }

                    }
                }
            }
        }


    }

    public function create()
    {
        $values = $this->validate();

        $values['member']['user_id'] = $this->user->id;
        $values['member']['user_picture'] = 'no_picture';
        $values['member']['user_hash'] = Str::random(4);

        $this->member = $member = UsersCredits::create($values['member']);

        $this->updateTags($values['tags'], $this->member);

        $this->member->user_hash = $this->member->id . '-' . $this->member->user_hash;
        $this->member->save();

        session()->flash('message', __('auth.record_created'));

        return redirect()->to(route('admin.members.member.edit', ['id' => $this->member->id]));
    }


    public function update()
    {
        $values = $this->validate();
        $this->updateTags($values['tags'], $this->member);
        $this->member->save();
        session()->flash('message', __('auth.record_udpate'));
    }

    public function mount ()
    {
        $id = request()->id;
        $this->user = auth()->user();
        $this->creditpackages = CreditsCategories::all();
        $this->tags = [];
        $this->member = [];
        $this->selectedTags = [];
        $this->corp = currcorp();

        $tags = Tags::where('corporations_id', $this->corp->id)->orderBy('id', 'DESC')->get();
        foreach($tags as $tag) {
            $this->tags[] = $tag->name;
        }

        if ($id > 0) {
            $this->member = UsersCredits::findOrFail($id);
            $selected_tags = MemberTags::where('users_credits_id', $this->member->id)->orderBy('id', 'DESC')->get();


            foreach($selected_tags as $member_tag) {
                $this->selectedTags[] = $member_tag->tag->name;
            }

        }
    }

    public function render()
    {

        return view('livewire.admin.member');
    }
}
