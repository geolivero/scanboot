<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Admin\Devices As DevicesModel;

class DeviceLocations extends Component

{
    use WithPagination;
    public $search = '';
    public $sort;
    public $sort_type = true;
    public $filter_list;
    public $corporation_id = 0;


    public function mount() {

        $this->filter_list = [[
            'label' => __('Name'),
            'value' => 'name'
        ]];

    }

    public function render()
    {
        $devices = [
            'devices' => DevicesModel::search('name', $this->search)->whereIn('corporations_id', [currcorp()->id])
                ->sort($this->sort, $this->sort_type)->paginate(20)
        ];
        return view('livewire.admin.device-locations', $devices);
    }
}
