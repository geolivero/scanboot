<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\UsersCredits;
use Livewire\WithPagination;
use App\Models\Admin\Tags;
use App\Models\Admin\Companies;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\MembersExport;

class Members extends Component
{
    use WithPagination;
    public $search = '';
    public $sort;
    public $sort_type = true;
    public $filter_list;
    public $corporation_id = 0;
    public $user_corporations;
    public $searched_tags;
    public $searched_companies;
    public $companies;
    public $tags;
    public $user;
    public $selected_members;
    public $allmembers;

    public function mount() {

        $this->filter_list = [[
            'label' => __('Name'),
            'value' => 'name'
        ]];
        $this->searched_companies = [];
        $this->searched_tags = [];
        $this->selected_members = [];
        $this->allmembers = [];
        $this->tags = Tags::where('corporations_id', currcorp()->id)->sort('name', 'ASC')->get();
        $this->companies = Companies::where('corporation_id', currcorp()->id)->sort('name', 'ASC')->get();
        $this->allmembers = UsersCredits::whereIn('company_id', companies()->ids)->get();

    }

    public function export($ext)
    {
        // abort_if($ext, ['csv', 'xlsx', 'pdf'], Response::HTTP_NOT_FOUND);
        $collection = $this->createCollection()->get();
        return Excel::download(new MembersExport($collection), 'data_members_exports_' . date('Y-m-d_H_i') . '.' . $ext);
    }


    private function createCollection ()
    {
        $searched_companies = $this->searched_companies;
        $searched_tags = $this->searched_tags;
        $selected_members = $this->selected_members;
        $results = UsersCredits::whereIn('company_id', companies()->ids);
        if (count($selected_members) > 0) {
            $results->whereIn('id', $selected_members);
        }
        $results->orderBy('updated_at', 'DESC')
            ->whereHas('company', function ($query) use ($searched_companies) {
                if (count($searched_companies) > 0) {
                    $query->whereIn('id', $searched_companies);
                }
            })
            ->whereHas('tags', function ($query) use ($searched_tags) {
                if (count($searched_tags) > 0) {
                    $query->whereIn('tags_id', $searched_tags);
                }
            })
            ->sort($this->sort, $this->sort_type);
        return $results;
    }

    public function render()
    {
        return view('livewire.admin.members', [
            'members' => $this->createCollection()->paginate(20),
            'total' => $this->createCollection()->count()
        ]);
    }
}
