<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\Admin\Roles as RolesModel;
use App\Models\Admin\UserRoles;

class Roles extends Component
{

    public $user_roles = [];
    public $roles;
    public $user;
    public $roles_id = [];


    protected $rules = [
        'user_roles' => 'required'
    ];

    public function update()
    {
        $values = $this->validate();

        $roles = isset($values['user_roles']) ? $values['user_roles'] : false;
        if ($roles) {
            UserRoles::where('user_id', $this->user->id)->delete();
            foreach($roles as $role) {
                UserRoles::create([
                    'user_id' => $this->user->id,
                    'roles_id' => $role
                ]);
            }
        }

        session()->flash('message', __('auth.record_udpate'));
    }

    public function create()
    {
        $values = $this->validate();

    }

    public function mount ($user)
    {
        $roles = RolesModel::all();
        $this->roles = [];


        if (\Admin::isRole('Admin')) {
            foreach($roles as $role) {
                $this->roles[] = [
                    'value' => $role->id,
                    'label' => $role->name
                ];
            }
        } else {
            foreach($roles as $role) {
                if ($role->name != 'Admin') {
                    $this->roles[] = [
                        'value' => $role->id,
                        'label' => $role->name
                    ];
                }
            }
        }




        $this->user = $user;
        if ($user) {
            $this->user_roles = UserRoles::where('user_id', '=', $user->id)->pluck('roles_id');
        }


    }

    public function render()
    {
        return view('livewire.admin.roles');
    }
}
