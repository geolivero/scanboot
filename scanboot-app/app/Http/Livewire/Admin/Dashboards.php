<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use Illuminate\Support\Str;
use App\Models\Admin\Devices;
use App\Models\DashboardToken;

class Dashboards extends Component
{
    public $dashboards;
    public $deviceLocations;
    public $deviceLocationId;
    private $page;

    protected $listeners = ['refreshComponent' => 'updateView'];


    public function mount ()
    {
        $this->page = request()->fullUrl();
        $this->deviceLocationId = 0;
        $this->deviceLocations = Devices::where('corporations_id', currcorp()->id)->get();
        $this->updateView();
    }

    public function updateView()
    {
        $this->dashboards = DashboardToken::where('corporations_id', currcorp()->id)->get();
    }

    public function render()
    {
        return view('livewire.admin.dashboards');
    }

    public function updatedDeviceLocationId($value)
    {
        $this->deviceLocationId = $value;
    }

    public function revoke($id)
    {
        DashboardToken::find($id)->delete();
        $this->emit('refreshComponent');
    }

    public function createToken()
    {
        $corp_id = currcorp()->id;
        $gen_token = Str::random(5);

        $token = DashboardToken::create([
            'token' => $gen_token,
            'devices_id' => $this->deviceLocationId,
            'corporations_id' => $corp_id
        ]);
        $token->token = $token->id .  $gen_token;
        $token->save();

        $this->emit('refreshComponent');

    }
}
