<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Admin\CreditsCategories as AdminCreditsCategories;

class CreditsCategories extends Component
{
    use WithPagination;
    public $search = '';
    public $sort;
    public $sort_type = true;
    public $filter_list;

    public function mount() {
        $this->filter_list = [[
            'label' => __('Name'),
            'value' => 'name'
        ]];

    }

    public function render()
    {
        $corp = currcorp();

        return view('livewire.admin.credits-categories', [
            'credits' => AdminCreditsCategories::search('name', $this->search)->where('corporations_id', $corp->id)
                ->sort($this->sort, $this->sort_type)->paginate(20)
        ]);
    }
}
