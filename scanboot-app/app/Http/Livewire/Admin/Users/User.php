<?php

namespace App\Http\Livewire\Admin\Users;

use Livewire\Component;
use App\Models\Admin\UserRoles;
use App\Models\Admin\Corporation;
use App\Models\User as ModelUser;
use Illuminate\Support\Facades\Hash;
use App\Models\Admin\UserCorporation;
use App\Notifications\WelcomeMessage;
use Illuminate\Support\Facades\Notification;



class User extends Component
{

    public $user;
    public $roles;
    public $selected_roles = [];
    public $user_roles;
    public $corporations;
    public $post;
    public $user_corporation = [];
    public $selected_corporations = [];

    protected $rules = [
        'user.name' => 'required|string|min:2',
        'user.email' => 'required|string|email|max:255',
        'selected_corporations' => 'array'
    ];

    public function confirmRemoval()
    {

        $this->user->delete();
        return redirect()->to('/admin/users');
    }

    private function updateUserCorporation ($corporations = []) {
        UserCorporation::where('user_id', $this->user->id)->delete();
        if (isset($corporations) && count($corporations) > 0) {
            foreach($corporations as $corp_id) {
                $results = UserCorporation::create([
                    'user_id' => $this->user->id,
                    'corporation_id' => intval($corp_id)
                ]);
            }
        }

        $this->setUserCorporation();
    }

    public function sendWelcome()
    {
        Notification::route('mail', $this->user->email)->notify(new WelcomeMessage($this->user));
        session()->flash('message', __('Welcome notification has been send'));
    }

    private function setUserCorporation() {
        $this->user_corporation = UserCorporation::where('user_id', $this->user->id)->get();
        $this->selected_corporations = [];
        foreach($this->user_corporation as $corp) {
            $this->selected_corporations[] = $corp->corporation_id;
        }
    }

    public function update()
    {
        $values = $this->validate();
        $this->updateUserCorporation($values['selected_corporations']);
        $this->user->save();

        session()->flash('message', __('auth.record_udpate'));
    }

    public function create()
    {
        $values = $this->validate();
        $values['user']['password'] = Hash::make('password', [
            'rounds' => 8,
        ]);

        $this->user = ModelUser::create($values['user']);
        $this->updateUserCorporation($values['selected_corporations']);


        session()->flash('message', __('auth.record_created'));
        return redirect()->to(route('admin.users.user', $this->user));
    }


    public function mount ()
    {
        $id = request()->id;
        $this->user_corporation = [];
        $this->corporations = Corporation::all();

        if ($id) {
            $this->user = ModelUser::findOrFail($id);
            $this->setUserCorporation();
        }
    }

    public function render()
    {
        return view('livewire.admin.users.user');
    }
}
