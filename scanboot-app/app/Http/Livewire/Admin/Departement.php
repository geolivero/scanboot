<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\Admin\Departements;

class Departement extends Component
{
    public $user;
    public $departement;
    public $corporations;
    public $corporation_id;

    protected $rules = [
        'departement.name' => 'required|string|min:2',
        'departement.company_id' => 'required|integer'
    ];


    public function confirmRemoval()
    {

        $this->departement->delete();
        return redirect()->to(route('admin.departements'));

    }

    public function create()
    {
        $values = $this->validate();
        $this->departement = Departements::create($values['departement']);

        session()->flash('message', __('auth.record_created'));
        return redirect()->to(route('admin.departements'));
    }



    public function update()
    {
        $values = $this->validate();
        $this->departement->save();
        session()->flash('message', __('auth.record_udpate'));
    }


    public function mount ()
    {
        $id = request()->id;
        $this->user = auth()->user();

        if ($id) {
            $this->departement = Departements::findOrFail($id);
        }
    }

    public function render()
    {
        return view('livewire.admin.departement');
    }
}
