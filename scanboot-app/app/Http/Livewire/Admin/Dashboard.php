<?php

namespace App\Http\Livewire\Admin;

use Carbon\Carbon;
use App\Models\Credits;
use Livewire\Component;
use Illuminate\Support\Str;
use App\Models\UsersCredits;
use App\Models\Admin\Devices;
use App\Models\DashboardToken;
use Asantibanez\LivewireCharts\Facades\LivewireCharts;

class Dashboard extends Component
{
    public $chartData = [];
    public $total_scans = 0;
    public $total_members = 0;
    public $token = '!!not generated!!';
    public $tokenItem;
    public $deviceLocations;
    public $deviceLocationId;

    public function mount ()
    {
        $this->tokenItem = [];
        $this->total_scans = Credits::where('updated_at', '>=', Carbon::now()->startOfMonth())
            ->whereIn('company_id', companies()->ids)
            ->get()->count();
        $this->total_members = UsersCredits::where('created_at', '>=', Carbon::now()->startOfMonth())
            ->whereIn('company_id', companies()->ids)
            ->get()->count();
        $this->deviceLocations = Devices::where('corporations_id', currcorp()->id)->get();
        $this->tokenItem = DashboardToken::where('corporations_id', currcorp()->id)->first();
        if ($this->tokenItem) {
            $this->token = $this->tokenItem->token;
        }
    }


    public function updatedDeviceLocationId($value)
    {
        $this->deviceLocationId = $value;
        $this->updateToken();
    }

    public function updateToken () {
        $corp_id = currcorp()->id;
        $gen_token = Str::random(5);

        if ($this->tokenItem) {
            $this->tokenItem->token = $this->tokenItem->id . $gen_token;
            $this->tokenItem->devices_id = $this->deviceLocationId;
            $this->tokenItem->save();
        } else {
            $this->tokenItem = DashboardToken::create([
                'token' => $gen_token,
                'devices_id' => $this->deviceLocationId,
                'corporations_id' => $corp_id
            ]);
            $this->tokenItem->token = $this->tokenItem->id .  $gen_token;
            $this->tokenItem->save();
        }
        $this->token = $this->tokenItem->token;
    }

    public function render()
    {

        $dateS = Carbon::now()->startOfMonth()->subMonth(4);
        $dateE = Carbon::now()->endOfMonth();

        $data = Credits::sort('updated_at', 'ASC')->get()
        ->whereBetween('updated_at',[$dateS,$dateE])
        ->whereIn('company_id', companies()->ids)
        ->groupBy(function($val) {
            return Carbon::parse($val->updated_at)->format('m');
        });

        $chart = LivewireCharts::ColumnChartModel();
        $chart->setTitle(__('Monthly usage'));
        $months = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEPT', 'OKT', 'NOV', 'DEC'];
        $colors = ['#7777E5','#77E593', '#77DAE5'];
        $index = 0;

        foreach($data as $key => $items) {
            $chart->addColumn($months[$key - 1], count($items), $colors[$index % 3]);
            $index += 1;
        }

        return view('livewire.admin.dashboard', [
            'chart' => $chart
        ]);
    }
}
