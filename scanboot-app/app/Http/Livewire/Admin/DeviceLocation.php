<?php

namespace App\Http\Livewire\Admin;

use App\Models\Admin\Devices;
use Livewire\Component;

class DeviceLocation extends Component
{

    public $device;


    protected $rules = [
        'device.name' => 'required|string|min:2'
    ];


    public function create()
    {
        $values = $this->validate();

        $this->device = Devices::create([
            'name' => $values['device']['name'],
            'corporations_id' => currcorp()->id
        ]);

        session()->flash('message', __('auth.record_created'));
        return redirect()->to(route('admin.devicelocations'));
    }

    public function confirmRemoval()
    {
        $this->device->delete();
        return redirect()->to(route('admin.devicelocations'));
    }


    public function update()
    {
        $values = $this->validate();
        $this->device->name = $values['name'];
        $this->device->save();
        session()->flash('message', __('auth.record_udpate'));
    }

    public function mount()
    {
        $id = request()->id;
        if ($id) {
            $this->device = Devices::findOrFail($id);
            $this->name = $this->device->name;
        }
    }

    public function render()
    {
        return view('livewire.admin.device-location');
    }
}
