<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Admin\UserCorporation;
use App\Models\Admin\Departements as AdminDepartements;

class Departements extends Component
{
    use WithPagination;
    public $search = '';
    public $sort;
    public $sort_type = true;
    public $filter_list;
    public $corporation_id = 0;
    public $user_corporations;
    public $user;

    public function mount() {

        $this->filter_list = [[
            'label' => __('Name'),
            'value' => 'name'
        ]];

    }

    public function render()
    {
        //corps();
        //companies()->get;
        //dd(companies()->ids);

        return view('livewire.admin.departements', [
            'departements' => AdminDepartements::search('name', $this->search)->whereIn('company_id', companies()->ids)
                ->sort($this->sort, $this->sort_type)->paginate(20)
        ]);
    }

}
