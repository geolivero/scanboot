<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Admin\Corporation As Corp;

class Corporations extends Component
{

    use WithPagination;
    public $search = '';
    public $sort;
    public $sort_type = true;
    public $filter_list;

    public function mount() {
        $this->filter_list = [[
            'label' => __('Name'),
            'value' => 'name'
        ]];
    }


    public function render()
    {

        return view('livewire.admin.corporations', [
            'corporations' => Corp::search('name', $this->search)
                ->sort($this->sort, $this->sort_type)->paginate(20)
        ]);
    }
}
