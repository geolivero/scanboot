<?php

namespace App\Http\Livewire\Admin;

use App\Models\Admin\Tags;
use Livewire\Component;

class Tag extends Component
{
    public $user;
    public $tag;

    protected $rules = [
        'tag.name' => 'required|string|min:2'
    ];


    public function confirmRemoval()
    {

        $this->tag->delete();
        return redirect()->to(route('admin.tags'));

    }

    public function create()
    {
        $values = $this->validate();
        $values['tag']['name'] = strtolower($values['tag']['name']);
        $values['tag']['corporations_id'] = currcorp()->id;

        $this->tag = Tags::create($values['tag']);

        session()->flash('message', __('auth.record_created'));
        return redirect()->to(route('admin.tags'));
    }



    public function update()
    {
        $values = $this->validate();
        $this->tag->name = strtolower($values['tag']['name']);
        $this->tag->save();
        session()->flash('message', __('auth.record_udpate'));
    }

    public function mount ()
    {
        $id = request()->id;

        if ($id) {
            $this->tag = Tags::findOrFail($id);
        }
    }


    public function render()
    {
        return view('livewire.admin.tag');
    }
}
