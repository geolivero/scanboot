<?php

namespace App\Http\Livewire\Admin;

use App\Models\Credits;
use Livewire\Component;
use App\Models\Admin\Tags;
use App\Exports\ScansExport;
use App\Models\UsersCredits;
use Livewire\WithPagination;
use App\Models\Admin\Companies;
use App\Models\Admin\Devices;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\Response;
use Asantibanez\LivewireCharts\Models\ColumnChartModel;

class Exports extends Component
{

    use WithPagination;
    public $search = '';
    public $sort;
    public $sort_type = true;
    public $filter_list;
    public $tags;
    public $searched_tags;
    public $searched_companies;
    public $companies;
    public $members;
    public $selected_members;
    public $selected_devices;
    public $date_from;
    public $date_to;
    public $collection;
    public $chart;
    public $devices;
    public $total_p_page;
    public $deleteItem;

    public function mount() {
        //date_default_timezone_set(currtimezone());

        $this->filter_list = [];
        $this->searched_companies = [];
        $this->searched_tags = [];
        $this->selected_members = [];
        $this->selected_devices = [];
        $this->tags = Tags::where('corporations_id', currcorp()->id)->sort('name', 'ASC')->get();
        $this->companies = Companies::where('corporation_id', currcorp()->id)->sort('name', 'ASC')->get();
        $this->members = UsersCredits::whereIn('company_id', companies()->ids)->sort('name', 'ASC')->get();
        $this->date_from = !$this->date_from ? date('Y-m-01\T00:00') : $this->date_from;
        $this->date_to = !$this->date_to ? date('Y-m-t\T23:59') : $this->date_to;
        $this->devices = Devices::where('corporations_id', currcorp()->id)->sort('name', 'ASC')->get();
        $this->collection = [];
        $this->total_p_page = 20;
    }

    public function export($ext)
    {
        // abort_if($ext, ['csv', 'xlsx', 'pdf'], Response::HTTP_NOT_FOUND);
        $collection = $this->createCollection()->get();
        return Excel::download(new ScansExport($collection), 'data_exports_' . date('Y-m-d_H_i') . '.' . $ext);
    }

    public function destroy($id)
    {
        $this->deleteItem = $id;
    }

    public function delete()
    {
        Credits::findOrFail($this->deleteItem)->delete();
        $this->deleteItem = 0;
        session()->flash('message', __('auth.remove'));
    }

    public function closeModal()
    {
        $this->deleteItem = 0;
    }

    private function createCollection ()
    {
        $searched_companies = $this->searched_companies;
        $searched_tags = $this->searched_tags;
        $selected_members = $this->selected_members;
        $selected_devices = $this->selected_devices;
        //$date = $credit->created_at;
        //date_timezone_set($date , timezone_open(currtimezone()));

        $credits =  Credits::whereIn('company_id', companies()->ids)
            ->where('updated_at', '>=', $this->date_from)
            ->where('updated_at', '<=', $this->date_to)
            ->orderBy('updated_at', 'DESC')
            //->groupBy('users_credits_id')
            //->select('users_credits_id', 'company_id', DB::raw('count(*) as total'))
            ->with(['usersCredits'])
            ->whereHas('usersCredits', function ($query) use ($selected_members) {
                if (count($selected_members) > 0) {
                    $query->whereIn('id', $selected_members);
                }
            })
            ->whereHas('usersCredits', function ($query) use ($searched_tags) {
                $newTags = $searched_tags;
                if (count($searched_tags) > 0) {
                    $query->whereHas('tags', function ($query) use ($newTags) {
                            $query->whereIn('tags_id', $newTags);
                    });
                }
            })
            ->whereHas('company', function ($query) use ($searched_companies) {
                if (count($searched_companies) > 0) {
                    $query->whereIn('id', $searched_companies);
                }
            })->sort($this->sort, $this->sort_type);
        if (count($selected_devices) > 0) {
            $credits->whereIn('device_id', $selected_devices);
        }

        return $credits;
    }

    public function render()
    {

        //dd($this->createCollection()->paginate(20));

        return view('livewire.admin.exports', [
            'scans' => $this->createCollection()->paginate($this->total_p_page),
            'total' => $this->createCollection()->count()
        ]);
    }
}
