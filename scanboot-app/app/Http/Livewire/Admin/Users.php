<?php

namespace App\Http\Livewire\Admin;

use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;

class Users extends Component
{

    use WithPagination;
    public $search = '';
    public $sort;
    public $sort_type = true;
    public $filter_list = [];

    public function mount(User $user) {
        $this->user = $user;

        $this->filter_list = [[
            'label' =>  __('Name'),
            'value' =>  'name'
        ],[
            'label' =>  __('Email'),
            'value' =>  'email'
        ]];
    }

    public function render()
    {
        return view('livewire.admin.users', [
            'users' => User::search('email', $this->search)->search('name', $this->search)
                ->sort($this->sort, $this->sort_type)->paginate(20)
        ]);
    }
}
