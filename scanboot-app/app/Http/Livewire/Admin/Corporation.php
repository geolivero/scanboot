<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\Admin\Corporation as Corp;
use Jackiedo\Timezonelist\Facades\Timezonelist;

class Corporation extends Component
{
    public $corporation;
    public $timezones;

    protected $rules = [
        'corporation.name' => 'required|string|min:2',
        'corporation.timezone' => 'required|string'
    ];


    public function confirmRemoval()
    {

        $this->corporation->delete();
        return redirect()->to(route('admin.corporations'));

    }

public function update()
    {
        $values = $this->validate();

        $this->corporation->save();


        session()->flash('message', __('auth.record_udpate'));
    }


    public function create()
    {
        $values = $this->validate();
        $this->corporation = Corp::create($values['corporation']);

        session()->flash('message', __('auth.record_created'));
        return redirect()->to(route('admin.corporations'));
    }


    public function mount ()
    {
        $id = request()->id;
        $this->timezones = Timezonelist::toArray();

        if ($id) {
            $this->corporation = Corp::findOrFail($id);
        }

    }

    public function render()
    {
        return view('livewire.admin.corporation');
    }
}
