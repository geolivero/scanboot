<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\Admin\Tags as TagsModel;
use Livewire\WithPagination;

class Tags extends Component
{
    use WithPagination;
    public $search = '';
    public $sort;
    public $sort_type = true;
    public $filter_list;

    public function mount() {
        $this->filter_list = [[
            'label' => __('Name'),
            'value' => 'name'
        ]];

    }

    public function render()
    {
        $corp = currcorp();

        return view('livewire.admin.tags', [
            'tags' => TagsModel::search('name', $this->search)->where('corporations_id', $corp->id)
                ->sort($this->sort, $this->sort_type)->paginate(20)
        ]);
    }
}
