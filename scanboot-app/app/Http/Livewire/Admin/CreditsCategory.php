<?php

namespace App\Http\Livewire\Admin;

use App\Models\Admin\CreditsCategories;
use Livewire\Component;

class CreditsCategory extends Component
{
    public $credit;

    protected $rules = [
        'credit.name' => 'required|string|min:2',
        'credit.credits' => 'required|integer|min:1',
    ];


    public function confirmRemoval()
    {

        $this->credit->delete();
        return redirect()->to(route('admin.credits'));

    }

    public function create()
    {
        $values = $this->validate();
        $values['credit']['corporations_id'] = currcorp()->id;
        $this->credit = CreditsCategories::create($values['credit']);
        session()->flash('message', __('auth.record_created'));
        return redirect()->to(route('admin.credits'));
    }



    public function update()
    {
        $values = $this->validate();
        $this->credit->save();
        session()->flash('message', __('auth.record_udpate'));
    }

    public function mount ()
    {
        $id = request()->id;

        if ($id) {
            $this->credit = CreditsCategories::findOrFail($id);
        }
    }

    public function render()
    {
        return view('livewire.admin.credits-category');
    }
}
