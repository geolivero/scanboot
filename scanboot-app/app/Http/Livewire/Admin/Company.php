<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use App\Models\Admin\Companies;

class Company extends Component
{

    public $user;
    public $company;
    public $corporations;
    public $corporation_id;
    public $timezones;

    protected $rules = [
        'company.name' => 'required|string|min:2',
        'company.location' => 'required|string|min:2',
        'company.corporation_id' => 'required|integer'
    ];


    public function confirmRemoval()
    {
        $this->company->delete();
        return redirect()->to(route('admin.companies'));
    }

    public function create()
    {
        $values = $this->validate();
        $this->setCorporation();
        $this->corporation = Companies::create($values['company']);

        session()->flash('message', __('auth.record_created'));
        return redirect()->to(route('admin.companies'));
    }



    public function update()
    {
        $this->setCorporation();
        $values = $this->validate();
        $this->company->save();
        session()->flash('message', __('auth.record_udpate'));
    }

    private function setCorporation ()
    {
        $this->corporations = [];
        if ($this->user->user_corporations) {
            foreach($this->user->user_corporations as $usrCorp) {
                $this->corporations[] = $usrCorp->corporation;
            }
        }
    }

    public function mount ()
    {
        $id = request()->id;
        $this->user = auth()->user();
        $this->setCorporation();
        if ($id) {
            $this->company = Companies::findOrFail($id);
        }
    }

    public function render()
    {
        return view('livewire.admin.company');
    }
}
