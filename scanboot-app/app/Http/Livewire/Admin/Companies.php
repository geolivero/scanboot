<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Support\Facades\Auth;
use App\Models\Admin\Companies As CompaniesModel;
use App\Models\Admin\UserCorporation;

class Companies extends Component

{
    use WithPagination;
    public $search = '';
    public $sort;
    public $sort_type = true;
    public $filter_list;
    public $corporation_id = 0;
    public $user_corporations;
    public $user;

    public function mount() {

        $this->filter_list = [[
            'label' => __('Name'),
            'value' => 'name'
        ]];

        $this->user = Auth::user();
    }

    public function render()
    {
        $user = auth()->user();
        $this->user_corporations = UserCorporation::where('user_id', $user->id)->get();
        $corporations = [];
        if ($this->user_corporations) {
            foreach($this->user_corporations as $user_corp) {
                $corporations[] = $user_corp->corporation->id;
            }
        }

        return view('livewire.admin.companies', [
            'companies' => CompaniesModel::search('name', $this->search)->whereIn('corporation_id', $corporations)
                ->sort($this->sort, $this->sort_type)->paginate(20)
        ]);
    }
}
