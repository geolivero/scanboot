<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin\Devices as DevicesModel;

class Devices extends Controller
{
    //

    public function index()
    {

        $devices = DevicesModel::where('corporations_id', '=',currcorp()->id)->orderBy('name')->get();

        return response()->json($devices, 200);
    }
}
