<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DashboardToken;
use App\Services\CreditService;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class DashboardCreditsView extends Controller
{
    //
    public function index ()
    {
        $token = session()->get('dashboard_token');
        if (!$token) {
            return redirect()->to(route('credits.dashboard'));
        }

        $dashtoken = DashboardToken::where('token', $token)->first();
        if (!$dashtoken) {
            return redirect()->to(route('credits.dashboard'));
        }

        return view('dashboard_view', [
            'token' => $token
        ]);
    }


    public function show ()
    {
        $token = session()->get('dashboard_token');
        $dashtoken = DashboardToken::where('token', $token)->first();


        if ($dashtoken && $dashtoken->corporations_id) {
            $scans = CreditService::getScanResultsByCorps($dashtoken->corporations_id, $dashtoken->devices_id);
            return response()->json($scans, 200);

            Log::channel('daily')->debug('Token is valid ' . $token);
            if (!$scans) {
                return response()->json([
                    'current' => [],
                    'credits' => []
                ], 200);
            } else {
                Log::channel('daily')->debug('NFC scaned user ' . $scans['current']['name']);
                return response()->json($scans, 200);
            }
        } else {
            Log::channel('daily')->debug('Token not valid ' . $token);
            return response()->json([
                'message' => __('Token is not valid, corporation not found')
            ], 402);
        }

    }
}
