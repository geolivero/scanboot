<?php

namespace App\Http\Controllers;

use App\Models\UsersCredits;
use Illuminate\Http\Request;
use App\Services\CreditService;
use Illuminate\Support\Facades\Log;
use App\Models\Credits as CreditsModel;
use GuzzleHttp\Promise\Create;
use Illuminate\Support\Facades\Session;

class Credits extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //TODO: Get the company ID from the logged in user
        $corp_id = currcorp()->id;

        try {
            $scans = CreditService::getScanResultsByCorps($corp_id);
            if (!$scans) {
                return response()->json([
                    'current' => [],
                    'credits' => []
                ], 200);
            }
            return response()->json($scans, 200);
        } catch (\Throwable $th) {
            report($th);
            return response()->json([
                'message' => 'Sorry something went wrong please contact support'
            ], 500);
        }

    }

    public function getByDeviceId($deviceid)
    {
        return response()->json($deviceid, 200);

        $corp_id = currcorp()->id;

        try {
            $scans = CreditService::getScanResultsByCorps($corp_id, $deviceid);
            if (!$scans) {
                return response()->json([
                    'current' => [],
                    'credits' => []
                ], 200);
            }
            return response()->json($scans, 200);
        } catch (\Throwable $th) {
            report($th);
            Log::error($th);
            return response()->json([
                'message' => 'Sorry something went wrong please contact support'
            ], 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $user = UsersCredits::where('user_hash', '=', $request->credit_hash)->first();
        $buy_credit = $request->use_credit;



        if (!$user) {
            return response()->json([
                'message' => __('Wrong key, serial code is not correct')
            ], 500);
        }



        $credits = CreditService::getUserCredits($user);
        if (!$credits) {
            return response()->json([
                'message' => __('No credits have been asigned to your account.')
            ], 500);
        }


        $already_scanned = CreditsModel::whereBetween('created_at', [now()->subMinutes(1), now()->subMinutes(-1)])
            ->where('users_credits_id', $user->id)
            ->first();


        if (!$already_scanned) {
            if ($request->device_id) {
                $credit = CreditsModel::firstOrCreate([
                    'users_credits_id' => $user->id,
                    'company_id' =>  $user->company_id,
                    'device_id' => $request->device_id,
                    'updated_at' => now()
                ]);
            } else {
                $credit = CreditsModel::firstOrCreate([
                    'users_credits_id' => $user->id,
                    'company_id' =>  $user->company_id,
                    'updated_at' => now()
                ]);
            }

            Log::channel('daily')->debug('Scan succesfull for user: ' . $user->name);
        } else {
            Log::channel('daily')->debug('Already scanned user found: ' . $user->name);
        }

        $scans_in_past = CreditsModel::where('created_at', '>', now()->subMinutes(5))->get();
        $scans_unique = $scans_in_past->unique(['users_credits_id']);
        $dup_scans_ids = $scans_in_past->diff($scans_unique)->pluck('id');

        if (count($dup_scans_ids) > 0) {
            CreditsModel::whereIn('id', $dup_scans_ids)->delete();
        }


        //dd(Carbon::parse('2021-11-10 17:14:18', 'America/Aruba')->tz(config('app.timezone')));
        $credits = CreditService::getUserCredits($user);

        if ($credits < 1) {
            return response()->json([
                'message' => __('Sorry your dont have any credits left')
            ], 500);
        }
        return response()->json([
            'credits' => $credits,
            'name' => $user->name
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
