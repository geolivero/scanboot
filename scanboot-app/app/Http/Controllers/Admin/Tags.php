<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Models\Admin\Tags as AdminTags;
use PhpParser\Node\Stmt\TryCatch;

class Tags extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        try {
            $validator = $request->validate([
                'name' => ['required', 'unique:tags', 'max:255']
            ]);

            $item = AdminTags::create($validator);
            return response()->json($item, 200);

            Log::channel('daily')->debug('tag created: ' . $validator['name']);

        } catch (\Throwable $th) {
            if (isset($th->validator)) {
                Log::channel('daily')->error('tag not created: ' . json_encode($th->validator->messages()));
                return response()->json([
                    'message' => $th->validator->messages()
                ], 500);
            } else {
                Log::channel('daily')->error('tag not created: ' . json_encode($th));
            }


            return response()->json([
                'message' => 'Sorry something went wrong please contact support'
            ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
