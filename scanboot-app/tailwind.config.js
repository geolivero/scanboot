const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    mode: 'jit',
    purge: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './vendor/laravel/jetstream/**/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
    ],

    "tailwindCSS.includeLanguages": {
        "blade": "html",
        "php": "html",
        "vue": "html",
        "plaintext": "html"
    },

    theme: {
        extend: {
            fontFamily: {
                sans: ['Nunito', ...defaultTheme.fontFamily.sans],
                poppins: ['Poppins', ...defaultTheme.fontFamily.sans],
            },
            maxWidth: {
                25: '25px',
                1120: '1120px'
            },
            minHeight: {
                800: '800px',
                500: '500px'
            },
            colors: {
                main: '#77DAE5',
                aqua: '#77DAE5',
                mineral: '#51619A',
                light: '#F5F9FE',
                dark: '#52545E'
            },
        },
    },

    plugins: [require('@tailwindcss/forms'), require('@tailwindcss/typography')],
};
