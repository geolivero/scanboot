<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MemberTags extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('member_tags', function (Blueprint $table) {
            $table->id();
            $table->foreignId('tags_id')->nullable()->index();
            $table->foreign('tags_id')->references('id')->on('tags')->onDelete('cascade');
            $table->foreignId('users_credits_id')->constrained();
            $table->foreignId('companies_id')->nullable()->index();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('member_tags');
    }
}
